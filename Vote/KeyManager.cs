﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Vote
{
    class KeyManager
    {
        private static String data = "voted";
        private static KeyManager instance = null;
        
        public static KeyManager getInstance()
        {
            if (instance == null)
            {
                instance = new KeyManager();
            }

            return instance;
        }


        //size만큼 랜덤으로 키생성
        public List<Key> genKeyList(int size)
        {
            List<Key> keylist = new List<Key>();
            int cnt = 0;
            Hashtable table = new Hashtable();
            while (cnt < size)
            {
 
                Key key = new Key();
                while(table.Contains(key.PrivKey))
                {
                    key = new Key();
                }
                table.Add(key.PrivKey, true);
                keylist.Add(key);
                cnt++;
            }
            return keylist;
        }



        //지문 스트링으로 키 생성
        public Key genKey(String fprint)
        {
            return new Key(fprint);
        }


        //사인이 제대로 된 값인지 되었는지 확인
        public bool verifySign(String sign, String pubkey)
        {

            byte[] publickey = StringToByteArray(pubkey);
            ECDsaCng key = new ECDsaCng(CngKey.Import(publickey, CngKeyBlobFormat.EccPublicBlob));

            return key.VerifyData(Encoding.UTF8.GetBytes(data), KeyManager.StringToByteArray(sign));
            
        }


        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            for (int i = 0; i < ba.Length; i++)
                hex.Append(ba[i].ToString("X2"));
            return hex.ToString();
        }

    }
}

