﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Vote
{
    class P2PReceiver : P2P
    {
        private static P2PReceiver instance = null;

        public static void startReceive()
        {
            if (instance == null)
            {
                instance = new P2PReceiver();
                instance.start();
            }
            else
            {
                instance.CancelAsync();
                instance.start();
            }
            Debug.WriteLine("Start P2P Receiv");
        }


        Socket socket;

        P2PReceiver()
        {
            this.WorkerReportsProgress = false;
            this.WorkerSupportsCancellation = true;
            this.DoWork += new DoWorkEventHandler(waitP2PCommand);
        }

        ~P2PReceiver()
        {
            if (socket != null)
            {
                socket.Close();
                socket = null;
            }
        }

        IPEndPoint getP2PEndPoint()
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, Network.P2P_PORT);
            
            Debug.WriteLine(Dns.GetHostName());
            Debug.WriteLine(endPoint.ToString());

            return endPoint;
        }
        

        void workByCommand(string command, Socket handler)
        {
            MessageBox.Show(command);

            switch (command)
            {

                case COMMAND_SEND_FILE:
                    //DBManager.getInstance().close();
                    MessageBox.Show("StartFile");
                    //sendDump(handler);

                    Stream stream = new NetworkStream(handler);
                    var bin = new BinaryFormatter();
                    bin.Serialize(stream, DBManager.getInstance().selectBlocks());
                    bin.Serialize(stream, DBManager.getInstance().selectTxesAll());

                    //sendMsg(handler, getBlocksData());
                    //DBManager.getInstance().init();
                    MessageBox.Show("EndFile");
                    break;

                case COMMAND_END:
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    break;
            }
        }

        
        
        void waitP2PCommand(object sender, DoWorkEventArgs e)
        {
            try
            {
                socket.Bind(getP2PEndPoint());
                socket.Listen(10);

                while (true)
                {
                    Debug.WriteLine("Listen Server Socket");

                    Socket handler = socket.Accept();

                    IPEndPoint remoteEndPoint = handler.RemoteEndPoint as IPEndPoint;
                    Debug.WriteLine("LISTEN Connected With = " + remoteEndPoint.Address);

                    string command = null;
                    
                    do
                    {
                        command = receiveMsg(handler);
                        MessageBox.Show("received COmmand " + command);
                        workByCommand(command, handler);
                    } while (!command.Equals(COMMAND_END));
                    
                }
            }
            catch (SocketException exception)
            {
                Debug.WriteLine(exception.ToString());
            }
        }
        
        void start()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.RunWorkerAsync();
        }
    }
}
