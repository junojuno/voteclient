﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Vote
{
    class BroadcastReceiver : BackgroundWorker
    {
        private const string COMMAND_NEW_PROB = "COMMAND_NEW_PROB";
        private const string COMMAND_JUDGE = "COMMAND_JUDGE";
        private const string COMMAND_NEW_BLOCK = "COMMAND_NEW_BLOCK";

        private int port;
        private Socket socket;
        private EndPoint serverEndPoint;

        public BroadcastReceiver(int port)
        {
            this.port = port;
            init();
        }

        ~BroadcastReceiver()
        {
            socket.Close();
            Debug.WriteLine("End Broadcast .....");
        }

        private void init()
        {
            this.DoWork += new DoWorkEventHandler(waitHashUpdateCommand);
        }


        void waitHashUpdateCommand(object sender, DoWorkEventArgs e)
        {

            while (true)
            {
                string msg = recvMsg();
                Debug.WriteLine("Received = " + msg);

               //s MessageBox.Show(msg);

                if (msg.Contains(P2PReceiver.COMMAND_NEW_TRANSACTION))
                {
                    string transaction = recvMsg();
                   // Debug.WriteLine("transaction = " + transaction);
                    JObject job = JObject.Parse(transaction);

                    NameValueCollection collections = new NameValueCollection();
                    
                    foreach (JProperty property in job.Children())
                    {
                        collections.Add(property.Name, property.Value.ToString());
                        Debug.WriteLine(property.Name + " / " + property.Value);
                    }

                    string sign = collections.Get("sign");
                    MessageBox.Show("sign is "+ sign);

                    TX tx = new TX(collections.Get("candidate"), sign, 0, 0);
                    

                    DBManager.getInstance().insertTransaction(tx);
                }
                else if (msg.Contains(COMMAND_NEW_PROB))
                {

                    //MessageBox.Show(msg);

                    string prob = recvMsg();
                    string tick = recvMsg();
                    

              //      Debug.WriteLine("New Prob Rece " + prob + " / " + tick)  ;
                    List<TX> txlist = getTxList(Int32.Parse(prob), Int32.Parse(tick));
                   // Debug.WriteLine("TXLIST HASH IS" + txlist.GetHashCode());

                    Block block = BlockManager.getInstance().makeBlock(getPrevHash(), txlist);
                    
                    BlockFinder finder = BlockFinder.startFind(block);
                    
                    finder.handler +=
                    (nonce, prevHash) => {
                     //   Debug.WriteLine("TXLIST HASH IS" + txlist.GetHashCode());

                        Debug.WriteLine("I Find it " + nonce +" / " + prevHash + " / " + BlockManager.getInstance().verifyHash(prevHash, txlist, nonce)) ;


                        Debug.WriteLine("judge data is " + TXManager.getInstance().getTXString(txlist));
                        JObject job = Network.getInstance().judge(block.Hash, prevHash, nonce + "", prob);

                       // Debug.WriteLine("Judge Return " + job.ToString());
                    };

                    //BlockManager.getInstance() .c .makeBlock()

                    // TODO After Find
                }
                else if (msg.Contains(COMMAND_JUDGE))
                {
                    string judgeData = recvMsg();

                    //MessageBox.Show("Judge Data is " + judgeData);
                    Debug.WriteLine("Judge Data = " + judgeData);

                    JObject job = JObject.Parse(judgeData);

                    NameValueCollection col = new NameValueCollection();

                    foreach (JToken token in job.Children())
                    {
                        JProperty prop = token as JProperty;

                        col.Add(prop.Name, prop.Value + "");
                    }

                    int prob = Int32.Parse(col.Get("prob"));

                    List<TX> txlis2 = getTxList(prob, 1);

                    bool isRight = BlockManager.getInstance().verifyHash(col.Get("prev_hash"), txlis2, Int32.Parse(col.Get("nonce")));

                    Debug.WriteLine("Judge JUDGE JUDGE " + isRight);


                    Network.getInstance().judgeRes(prob, isRight ? 1 : 0);
                    MessageBox.Show("rejudge data is " + TXManager.getInstance().getTXString(txlis2));

                    Debug.WriteLine( "rejudge data is " + TXManager.getInstance().getTXString(txlis2));

                }
                else if (msg.Contains(COMMAND_NEW_BLOCK))
                {
                    string newBlockData = recvMsg();
                    Debug.WriteLine("New BLock Data is " + newBlockData.ToString());

                    JObject job = JObject.Parse(newBlockData);
                    NameValueCollection col = new NameValueCollection();

                    foreach (JToken token in job.Children())
                    {
                        JProperty prop = token as JProperty;
                        col.Add(prop.Name, prop.Value.ToString());
                    }


                    DBManager.getInstance().insertBlock(col.Get("hash"), col.Get("prev_hash"));
                }
                //new P2PSender(msg, null, P2P.COMMAND_SEND_FILE).RunWorkerAsync();
            }
            
        }

        private void BroadcastReceiver_handler()
        {
            throw new NotImplementedException();
        }

        private List<TX> getTxList(int prob, int tick)
        {
            return DBManager.getInstance().getTxBetween(prob, prob);
        }

        private string getPrevHash()
        {
            return DBManager.getInstance().prevHash();
        }

        public string recvMsg()
        {
            byte[] recBuffer = new byte[512];
            int cnt = socket.ReceiveFrom(recBuffer, ref serverEndPoint);
            string msg = Encoding.UTF8.GetString(recBuffer, 0, cnt);
            return msg;
        }

        public void start()
        {
            Debug.WriteLine("Receive Broadcast .....");

            IPEndPoint myEndPoint = new IPEndPoint(IPAddress.Any, port);
            IPEndPoint serverIpEndPoint = new IPEndPoint(IPAddress.None, 0);
            serverEndPoint = serverIpEndPoint;

            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            socket.Bind(myEndPoint);
            this.RunWorkerAsync();
        }
    }
}
