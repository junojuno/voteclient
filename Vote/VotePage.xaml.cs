﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// VotePage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class VotePage : Page
    {

        private int election_idx;
        private Candidate candidate;
        private JArray jar;
        private string voterName;

        public VotePage()
        {
            InitializeComponent();
        }

        public VotePage(string name, int election_idx, JArray jar)
        {
            InitializeComponent();
            this.voterName = name;
            this.election_idx = election_idx;
            this.jar = jar;
            loadCandidates(election_idx);
        }

        public void loadCandidates(int election_idx)
        {
            List<Candidate> candidates = DBManager.getInstance().selectCandidates(election_idx);
            lbCandidates.ItemsSource = candidates;
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (lbCandidates.SelectedItems.Count > 0)
            {
                candidate = (Candidate)(lbCandidates.SelectedItem);

            //    String transaction = String.Format(DBManager.QUERY_ADD_TRANSACTION,)

                VoteQueue.getInstance().addQueueWork(
                    (ns, ne) =>
                    {
                        string sign = new Key().getSign();

                        JObject job = new Network().vote(voterName, sign, election_idx, candidate.idx, jar.ToString()) ;

                        string success = job.GetValue("success").ToString();

                        Debug.WriteLine(success + " SSSS " + job.ToString());

                        if (success.Equals("False"))
                        {
                            MessageBox.Show("Failed To Vote, Check your Number");
                        } else
                        {
                            MessageBox.Show("Success To Vote");
                        }
                        
                    }
                );
                NavigationService.Navigate(new MainPage());
                //MessageBox.Show(candidate.number + " / " + candidate.name);
            }
        }
        
    }
}
