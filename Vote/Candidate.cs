﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    class Candidate
    {
        public int idx { get; set; }
        public string name { get; set; }
        public string promise { get; set; }
        public string info { get; set; }
        public int idx_elections { get; set; }
        public String _img;
        public int number { get; set; }
        public int cntVote { get; set; }

        public string img
        {
            get
            {
                return Network.RESOURCES_URL + this._img;
            }
            set { this._img = value; }
        }
        

    }
}
