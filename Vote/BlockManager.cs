﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;

namespace Vote
{
    class BlockManager : BackgroundWorker
    {
        private static BlockManager instance = null;
        public static BlockManager getInstance()
        {
            if (instance == null)
            {
                instance = new BlockManager();

            }
            return instance;
        }

        //새로운 블럭 생성(해시값이 확정되기 전)
        public Block makeBlock(String prevHash, List<TX> txlist)
        {
            return new Block(KeyManager.StringToByteArray(prevHash), txlist);
        }


        //해시값이 정해진 블럭을 다시 생성하여 확인할 때 사용
        private Block makeBlock(String prevHash, List<TX> txlist, String TXHash, UInt32 nonce)
        {
            byte[] prev = KeyManager.StringToByteArray(prevHash);
            byte[] TX = KeyManager.StringToByteArray(TXHash);
            BlockHeader header = new BlockHeader(prev, TX, nonce);
            return new Block(header, txlist);
        }


        //블록의 해시값을 string으로 반환
        public String makeHash(Block block)
        {
            return KeyManager.ByteArrayToString(block.getHash());
        }





        //유효한 해시값이 나올 때 까지 순서대로 반복 사용X
        public Block findHash(Block block)
        {
            bool found = false;
            string hash;
            block.header.nNonce = 0;
            for (int i = 0; i <= 0xfffffff; i++)
            {
                hash = makeHash(block);
                block.header.nNonce++;
                found = true;
                found = verifyHash(hash);
                /*  for (int j = 0; j < 5 && found; j++)
                  {
                      if (hash.Substring(0,5) != "00000") found = false;
                  }*/

                if (found)
                {
                    Console.WriteLine(hash);
                    return block;
                }
            }
            block.header.nNonce = 0;
            return block;
        }


        //유효한 해시값이 나올 때 까지 랜덤값으로 반복 - BlockFinder를 통해 사용
        public Block findHashRand(Block block)
        {
            bool found = false;
            Random r = new Random();
            string hash;
            block.header.nNonce = 0;
            for (int i = 0; i <= 0xfffffff; i++)
            {
               
                block.header.nNonce = (UInt32)r.Next(0, 0xfffffff);
                hash = makeHash(block);
                found = true;
                found = verifyHash(hash);
                if (found)
                {
                    HashManager hashm = HashManager.getInstance();
                   // Debug.WriteLine("asdfasdf : " + KeyManager.ByteArrayToString(hashm.S256Hash("asdfasdf")));
                    Debug.WriteLine("hash found:  "+hash);
                    Debug.WriteLine("header:  " + block.header.ToString());
                    return block;
                }
            }

            block.header.nNonce = 0;
            return block;
        }

        //블록의 실제 해시값과 비교
        private bool compareHash(Block block, String hash)
        {
            return verifyHash(hash) && makeHash(block).CompareTo(hash) == 0;

        }

        // Verify
        public bool verifyHash(String prevHash, List<TX> txlist, int nonce)
        {
            Block block = makeBlock(prevHash, txlist);
            block.header.nNonce = (UInt32)nonce;
            String hash = makeHash(block);
            Debug.WriteLine("verify  "+hash);
            Debug.WriteLine("header:  " + block.header.ToString());

            return verifyHash(hash);
        }

        private bool verifyHash(String hash)
        {
            return hash.Substring(0, 4) == "0000";
        }

        public Hashtable cntVote(List<Block> blocklist)
        {
            Hashtable cnt = new Hashtable();
            Block[] arr = blocklist.ToArray();
            String prev = makeHash(arr[arr.Length - 1]), pub;
            for (int i = arr.Length - 1; i >= 0; i--)
            {
                if (prev != makeHash(arr[i])) ;
                prev = arr[i].PrevHash;
                foreach (TX tx in arr[i].TXList)
                {
                    pub = tx.PubKey;
                    if (cnt.Contains(pub)) cnt[pub] = (int)cnt[pub] + 1;
                    else cnt[pub] = 1;
                }
            }


            return cnt;

        }


        public Hashtable cntVote(List<TX> txlist)
        {
            Hashtable cnt = new Hashtable();
            String pub;
             foreach (TX tx in txlist)
             {
                   pub = tx.PubKey;
                    if (cnt.Contains(pub)) cnt[pub] = (int)cnt[pub] + 1;
                    else cnt[pub] = 1;
            }


            return cnt;

        }
    }

}