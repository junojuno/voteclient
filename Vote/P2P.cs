﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    class P2P : BackgroundWorker
    {

        public const string COMMAND_NEW_TRANSACTION = "COMMAND_NEW_TRANSACTION";
        public const string COMMAND_TEST = "COMMAND_TEST";
        public const string COMMAND_SEND_FILE = "COMMAND_SEND_FILE";
        public const string COMMAND_UPDATE_HASH = "COMMAND_UPDATE_HASH";

        public const string COMMAND_READY = "COMMAND_READY";
        public const string COMMAND_OK = "COMMAND_OK";
        public const string COMMAND_END = "COMMAND_END";

        public void sendDump(Socket handler)
        {
            DBManager.getInstance().close();
            FileStream fileStream = new FileStream(DBManager.DATABASE_NAME, FileMode.Open, FileAccess.Read);
            int fileLength = (int)fileStream.Length;

            byte[] buffer = BitConverter.GetBytes(fileLength);

            handler.Send(buffer);

            int count = fileLength / 1024 + 1;

            BinaryReader reader = new BinaryReader(fileStream);

            for (int i = 0; i < count; i++)
            {
                buffer = reader.ReadBytes(1024);
                handler.Send(buffer);
            }

            reader.Close();

            DBManager.getInstance().init();
        }

        public void receiveDump(Socket handler)
        {

            DBManager.getInstance().close();

            byte[] buffer = new byte[4];

            handler.Receive(buffer);

            int fileLen = BitConverter.ToInt32(buffer, 0);

            buffer = new byte[1024];

            int totalLen = 9;

            FileStream fileStream = new FileStream(DBManager.DATABASE_NAME, FileMode.Create, FileAccess.Write);
            BinaryWriter writer = new BinaryWriter(fileStream);

            while (totalLen < fileLen)
            {
                int receivedLen = handler.Receive(buffer);

                writer.Write(buffer, 0, receivedLen);

                totalLen += receivedLen;
            }

            fileStream.Close();
            writer.Close();


            DBManager.getInstance().init();
        }

        protected string receiveMsg(Socket handler)
        {
            string data = null;

            while (true)
            {
                byte[] bytes = new byte[1024];
                int bytesRec = handler.Receive(bytes);
                data += Encoding.ASCII.GetString(bytes, 0, bytesRec);

                if (data.IndexOf("") > -1)
                {
                    break;
                }
            }
            Debug.WriteLine(String.Format("LISTEN Received Data  = {0}", data));

            return data;
        }

        protected int sendMsg(Socket handler, string data)
        {
            byte[] msg = Encoding.Default.GetBytes(data);
            int res = handler.Send(msg);
            Debug.WriteLine("send with = " + data);
            return res;
        }
    }
}
