﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    [Serializable]
    class BlockHeader
    {

        public byte[] prevHash;
        public byte[] TXHash;
        public UInt32 nNonce;
        public BlockHeader(byte[] prev, byte[] tx)
        {
            prevHash = prev;
            TXHash = tx;
            nNonce = 0;
        }
        public BlockHeader(byte[] prev, byte[] tx, UInt32 nonce)
        {
            prevHash = prev;
            TXHash = tx;
            nNonce = nonce;
        }
        public byte[] getHash()
        {
            HashManager hashm = HashManager.getInstance();
            String str = "{" + KeyManager.ByteArrayToString(prevHash) + ", " + KeyManager.ByteArrayToString(TXHash) + ", " + nNonce + "}";
            return hashm.S256Hash(HashManager.ObjectToByteArray(str));
        }
        
        override public String ToString()
        {
            String str = "{" + KeyManager.ByteArrayToString(prevHash) + ", " + KeyManager.ByteArrayToString(TXHash) + ", " + nNonce + "}";
            return str;

        }

    }
    class Block
    {
        public BlockHeader header;
        private List<TX> tx = new List<TX>();


        public Block(byte[] prev, List<TX> txlist)
        {
            TXManager txm = TXManager.getInstance();
            HashManager hashm = HashManager.getInstance();
            string txstr = txm.getTXString(txlist);
            this.header = new BlockHeader(prev, hashm.S256Hash(txstr));
            this.tx = txlist;
        }
        public Block(BlockHeader header, List<TX> txlist)
        {
            this.header = header;
            this.tx = txlist;
        }
        public byte[] getHash()
        {
            return header.getHash();
        }

        public String Hash
        {
            get { return KeyManager.ByteArrayToString(header.getHash()); }
        }

        public String PrevHash
        {
            get { return KeyManager.ByteArrayToString(header.prevHash); }
        }
        public String TXHash
        {
            get { return KeyManager.ByteArrayToString(header.TXHash); }
        }
        public UInt32 Nonce
        {
            get { return header.nNonce; }
        }
        public List<TX> TXList
        {
            get { return tx; }
        }
        /*
        static void Main(string[] args)
        {
            HashManager hashm = HashManager.getInstance();
            List < TX > txlist = new List<TX>();
            txlist.Add(new TX("12", "12", 1));
            Block b = new Block(hashm.S256Hash(HashManager.ObjectToByteArray("hello")),txlist);
            BlockManager blockm = BlockManager.getInstance();
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(blockm.makeHash(blockm.findHash(b)));
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(blockm.makeHash(blockm.findHashRand(b)));
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(blockm.makeHash(blockm.findHashRand(b)));
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(blockm.makeHash(blockm.findHashRand(b)));
            Console.WriteLine(DateTime.Now);
            
            byte[] hash;
             * 
            while(true)
            {
                hash = hashm.S256Hash(HashManager.ObjectToByteArray(b.header));
                //Console.WriteLine(KeyManager.ByteArrayToString(hash));
                b.header.nNonce++;
                int i;
                for (i = 0; i < 5; i++)
                {
                    if (hash[i] != 0)
                        break;
                    if (b.header.nNonce > 2000000000)
                        break;
                }
                if (i >= 2)
                    Console.WriteLine(KeyManager.ByteArrayToString(hash));
            }
        }*/


    }

}