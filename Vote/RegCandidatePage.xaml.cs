﻿using Microsoft.Win32;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// RegCandidatePage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RegCandidatePage : Page
    {
        int elections_idx;

        public RegCandidatePage(int elections_idx)
        {
            InitializeComponent();
            this.elections_idx = elections_idx;
        }

        public RegCandidatePage()
        {
            InitializeComponent();
        }

        private void btnCandidateImg_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = " Open Image";
            dlg.Filter = "Image Files (*.jpg;*.jpeg;*.png) | *.JPG;*.JPEG;*.PNG";
            dlg.ShowDialog();
            lbImgPath.Content = dlg.FileName;
        }

        private void btnComplete_Click(object sender, RoutedEventArgs e)
        {
            NameValueCollection param = new NameValueCollection();
            NameValueCollection image = new NameValueCollection();

            Key key = KeyManager.getInstance().genKeyList(1)[0];

            param.Add("name", tbName.Text);
            param.Add("promise", tbPromise.Text);
            param.Add("info", tbInfo.Text);
            param.Add("idx_elections", elections_idx + "");
            param.Add("public_key", key.PubKey);

            image.Add("img", lbImgPath.Content.ToString());

            JObject job= new Network().addCandidate(param, image);
            Debug.WriteLine(job);

            NavigationService.Navigate(new MainPage());
        }
    }
}
