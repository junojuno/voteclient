﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// ResultPage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ResultPage : Page
    {
        public ResultPage()
        {
            InitializeComponent();
        }

        public ResultPage(int election_idx)
        {
            InitializeComponent();

            Hashtable table = BlockManager.getInstance().cntVote(DBManager.getInstance().selectTxesAll());

            foreach (string key in table.Keys)
            {

                // foreach (Candidate cand in candidates)
                //  {
                //      cand.
                //   }

                //table[key] = Int32.Parse(table[key].ToString()) + 1;
                Debug.WriteLine("keyval is " + key + " / " + table[key]);
            }

            // List<Candidate> candidates = DBManager.getInstance().selectCandidates(election_idx);
            JObject job = Network.getInstance().candidateList(election_idx);
            JArray jar = JArray.Parse(job.First.First.ToString());

            List<Candidate> candidates = new List<Candidate>();
            foreach (JToken child in jar.Children())
            {

                NameValueCollection col = new NameValueCollection();

                foreach (JToken grandChild in child.Children())
                {
                    JProperty prop = grandChild as JProperty;
                    col.Add(prop.Name, prop.Value + "");

                }

                Candidate can = new Candidate();
                can.number = Int32.Parse((col.Get("number").ToString()));
                can.img = col.Get("img");
                can.name = col.Get("name");


                if (table.Contains(col.Get("public_key")))
                {
                    can.cntVote = (int)table[col.Get("public_key")];

                }

                candidates.Add(can);
            }
            

            

            lbResults.ItemsSource = candidates;
        }
        
    }
}
