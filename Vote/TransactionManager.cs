﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Vote
{
    class TXManager
    {
        private static TXManager instance = null;
        private static Key admin;
        public static TXManager getInstance()
        {
            if (instance == null)
            {
                instance = new TXManager();
                admin = new Key();
            }

            return instance;
        }

        public List<TX> genTXList(List<Key> keylist)
        {
           return genTXList(admin, keylist);
        }
        private List<TX> genTXList(Key admin, List<Key> keylist)
        {
            List<TX> txlist = new List<TX>();
            
            foreach (Key key in keylist)
            {
                txlist.Add(genTX(key));
            }

            return txlist;
        }

        //처음 표 생성
        public TX genTX(Key key)
        {
            TX tx = new TX(key.PubKey,admin.getSign(), -1);
            return tx;
        }


        //투표할 때 트랜젝션 생성
        public TX genTX(Key key, String candidate, int prevID)
        {
            TX tx = new TX(candidate, key.getSign(), prevID);
            return tx;
        }
        public string getTXString(List<TX> txlist)
        {
            string str = "{";
            foreach( TX tx in txlist)
            {
                str += tx.ToString();
            }
            str += "}";
            return str;
        }
        
    }
}
