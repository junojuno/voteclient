﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    class P2PSender : P2P
    {

        
        string address;
        string data;
        string command;

        public P2PSender(string address, string data, string command)
        {
            this.address = address;
            this.data = data;
            this.command = command;
            init();
        }

        public P2PSender()
        {
            init();
        }

        void init ()
        {
            this.WorkerReportsProgress = false;
            this.WorkerSupportsCancellation = true;

            switch (command)
            {
                case COMMAND_TEST:
                    this.DoWork += new DoWorkEventHandler(testConnectWith);
                    break;
                case COMMAND_SEND_FILE:
                    this.DoWork += new DoWorkEventHandler(requestDbDump);
                    break;
            }
            
        }

        public void requestDbDump(object sender, DoWorkEventArgs e)
        {

            IPAddress ipAddress = IPAddress.Parse(address);
            IPEndPoint endPoint = new IPEndPoint(ipAddress, Network.P2P_PORT);

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);

            Debug.WriteLine("connected with = " + address);

            DBManager.getInstance().close();

            sendMsg(socket, COMMAND_SEND_FILE);

            Stream stream = new NetworkStream(socket);
            var bin = new BinaryFormatter();

            var blocks = (List<Block>)bin.Deserialize(stream);
            var txes = (List<TX>)bin.Deserialize(stream);

            DBManager.getInstance().deleteBlks();
            DBManager.getInstance().deleteTxes();

            foreach(Block block in blocks)
            {
                DBManager.getInstance().insertBlock(block.Hash, block.PrevHash);
            }

            foreach (TX tx in txes)
            {
                DBManager.getInstance().insertTransaction(tx);
            }

            sendMsg(socket, COMMAND_END);
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();

            DBManager.getInstance().init();
        }
        

        void testConnectWith(object sender, DoWorkEventArgs e)
        {
            try {
                IPAddress ipAddress = IPAddress.Parse(address);
                IPEndPoint endPoint = new IPEndPoint(ipAddress, Network.P2P_PORT);

                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(endPoint);

                Debug.WriteLine("connected with = " + address);

                sendMsg(socket, COMMAND_TEST);
                string msg = receiveMsg(socket);

                Debug.WriteLine("msg = " + msg);

                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            } catch (SocketException ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            //IPHostEntry ipHostInfo = Dns.GetHostAddresses(IPAddress.Parse("127.0.0.1"));
        }


    }
}
