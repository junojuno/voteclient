﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Vote
{
    class Key
    {
        private static String data = "voted";
        private byte[] privatekey, publickey;
        private ECDsaCng eckey;
        public Key()
        {
            CngKeyCreationParameters keyCreationParameters = new CngKeyCreationParameters();
            keyCreationParameters.ExportPolicy = CngExportPolicies.AllowExport;
            keyCreationParameters.ExportPolicy = CngExportPolicies.AllowPlaintextExport;
            keyCreationParameters.KeyUsage = CngKeyUsages.Signing;
            CngKey key = CngKey.Create(CngAlgorithm.ECDsaP256, null, keyCreationParameters);
            eckey = new ECDsaCng(key);

            publickey = eckey.Key.Export(CngKeyBlobFormat.EccPublicBlob);
            privatekey = eckey.Key.Export(CngKeyBlobFormat.EccPrivateBlob);
        }
        /*
        public Key(String fprint)
        {
            HashManager hashm = HashManager.getInstance();
            byte[] priv = Encoding.UTF8.GetBytes(fprint);

            privatekey = hashm.S256Hash(priv);
            eckey = new ECDsaCng(CngKey.Import(privatekey, CngKeyBlobFormat.EccPrivateBlob));
            publickey = eckey.Key.Export(CngKeyBlobFormat.EccPublicBlob);

        }*/
        
        public Key(String priv)
        {
            privatekey = KeyManager.StringToByteArray(priv);
            eckey = new ECDsaCng(CngKey.Import(privatekey, CngKeyBlobFormat.EccPrivateBlob));
            publickey = eckey.Key.Export(CngKeyBlobFormat.EccPublicBlob);

        }
        
       
        public String PubKey
        {
            get
            {
                return KeyManager.ByteArrayToString(publickey);
            }
        }
        public String PrivKey
        {
            get
            {
                return KeyManager.ByteArrayToString(privatekey);
            }
        }
        public String getSign()
        {
            byte[] signature = eckey.SignData(Encoding.UTF8.GetBytes(data));
            return KeyManager.ByteArrayToString(signature);
        }
        /* public int verifySign(String sign)
         {
            // if (eckey.VerifyData(StringToByteArray("voted"), StringToByteArray(sign)))
             if (eckey.VerifyData(data, KeyManager.StringToByteArray(sign)))
                 return 0;//data is good
             return -1;//data is wrong
         }*/


    }
}