﻿using Newtonsoft.Json.Linq;
using PatternRecognition.FingerprintRecognition.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// AuthPage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class AuthPage : Page
    {
        public enum Redirect
        {
            VOTE, REG_VOTER, REG_CANDIDATE, RES
        }

        private Bitmap fingerprint;
        private int election_idx;
        private Redirect redirect;
        private JArray jar;
        int cntBurification;


        public AuthPage()
        {
            InitializeComponent();
        }

        public AuthPage(int election_idx, Redirect redirect)
        {
            InitializeComponent();
            this.election_idx = election_idx;
            this.redirect = redirect;
        }

        public string MD5Hash(string Data)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(Encoding.ASCII.GetBytes(Data));

            StringBuilder stringBuilder = new StringBuilder();
            foreach (byte b in hash)
            {
                stringBuilder.AppendFormat("{0:x2}", b);
            }
            return stringBuilder.ToString();
        }


        public void authComplete()
        {
            if (redirect == Redirect.VOTE)
            {
                NavigationService.Navigate(new VotePage(MD5Hash(tbName.Text), election_idx, jar));
            }
            else if (redirect == Redirect.REG_VOTER)
            {
                //string name = Microsoft.VisualBasic.Interaction.InputBox("이름을 입력해주세요", "유권자 등록", "이름", -1, -1);
                
                JObject job = new Network().addVoter(MD5Hash(tbName.Text), jar.ToString(), election_idx + "");

                /*
                List<Key> keys = KeyManager.getInstance().genKeyList(1);
                job = new Network().updateNodeInfo(election_idx + "", keys);
                
                TX tx = TXManager.getInstance().genTX(keys[0]);
    */            

                NavigationService.Navigate(new MainPage());
            }
          
        }

        private void btnSkip_Click(object sender, RoutedEventArgs e)
        {
            authComplete();
        }

        private void btnAuth_Click(object sender, RoutedEventArgs e)
        {
            if (fingerprint == null || jar.Count < 15)
            {
                btnAuth.Content = "ing...";
                
                Thread task = new Thread(new ThreadStart(
                    () =>
                    {
                        ImageProcessor.getInstance().imageRequest();
                        fingerprint = ImageProcessor.getInstance().imageSave("fingerprint.bmp");
                        List<Minutia> minuates = new FingerMinutiaeExtractor().ExtractFeatures(fingerprint);

                        jar = new JArray();
                        cntBurification = 0;

                        foreach (Minutia item in minuates)
                        {
                            JObject job = new JObject();
                            job.Add("angle", item.Angle);
                            job.Add("x", item.X);
                            job.Add("y", item.Y);
                            job.Add("MinutiaType", item.MinutiaType.ToString());
                            job.Add("flag", item.Flag);

                            jar.Add(job);

                            if (item.MinutiaType == MinutiaType.Bifurcation)
                            {
                                cntBurification++;
                            }
                        }
                        Debug.WriteLine(jar);
                    }));

                task.Start();
                task.Join();
                
                btnAuth.Content = "Complete";
                imgFinger.Source = ConvertBitmap(fingerprint);
                label.Visibility = Visibility.Hidden;

                if (jar.Count < 15 && cntBurification < 3)
                {
                    btnAuth.Content = "Ready";
                    MessageBox.Show("지문을 다시 등록하여주십시오");
                }
                
            }
           
            else
            {
                authComplete();
            }
        }

        public static BitmapSource ConvertBitmap(Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                          source.GetHbitmap(),
                          IntPtr.Zero,
                          Int32Rect.Empty,
                          BitmapSizeOptions.FromEmptyOptions());
        }



    }
}
