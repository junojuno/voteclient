﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// VotePage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ElectionListPage : Page
    {
        AuthPage.Redirect redirect;

        public ElectionListPage(AuthPage.Redirect redirect)
        {
            this.redirect = redirect;
            InitializeComponent();
            initVoteList();
        }

        public ElectionListPage()
        {
            InitializeComponent();
            initVoteList();
        }

        void initVoteList()
        {
            List<Election> elections = DBManager.getInstance().selectElections();
            lvElections.ItemsSource = elections;
        }

        private void lvElections_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Election election = (Election)(lvElections.SelectedItem);

                if (redirect == AuthPage.Redirect.REG_CANDIDATE)
                {
                    NavigationService.Navigate(new RegCandidatePage(election.idx));
                }
                else if (redirect == AuthPage.Redirect.RES) {
                    NavigationService.Navigate(new ResultPage(election.idx));
                }
                else
                {
                    NavigationService.Navigate(new AuthPage(election.idx, redirect));
                }

            }
        }
    }
}