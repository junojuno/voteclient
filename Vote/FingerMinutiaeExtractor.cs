﻿

using ImageProcessingTools;
using PatternRecognition.FingerprintRecognition.Core;
using System;
using System.Collections.Generic;
using System.Drawing;

public class FingerMinutiaeExtractor : FeatureExtractor<List<Minutia>>
{
    public override List<Minutia> ExtractFeatures(Bitmap image)
    {
        FingerOrImgExtractor orImgExtractor = new FingerOrImgExtractor();
        OrientationImage orientationImage = orImgExtractor.ExtractFeatures(image);
        ImageMatrix matrix = skeImgExtractor.ExtractSkeletonImage(image, orientationImage);

        return GetMinutiaes(matrix, orientationImage);
    }

    #region private

    private List<Minutia> GetMinutiaes(ImageMatrix matrix, OrientationImage orientationImage)
    {
        List<Minutia> minutiaes = new List<Minutia>();

        for (int row = 0; row < orientationImage.Height; row++)
            for (int col = 0; col < orientationImage.Width; col++)
                if (!orientationImage.IsNullBlock(row, col))
                {

                    int x, y;
                    orientationImage.GetPixelCoordFromBlock(row, col, out x, out y);

                    int maxLength = orientationImage.WindowSize / 2;

                    for (int xi = x - maxLength; xi < x + maxLength; xi++)
                        for (int yi = y - maxLength; yi < y + maxLength; yi++)
                            if (xi > 0 && xi < matrix.Width - 1 && yi > 0 && yi < matrix.Height - 1)
                            {
                                if (matrix[yi, xi] == 0)
                                {
                                    List<int> values = new List<int>
                                                            {
                                                                matrix[yi, xi + 1] == 255 ? 0 : 1,
                                                                matrix[yi - 1, xi + 1] == 255 ? 0 : 1,
                                                                matrix[yi - 1, xi] == 255 ? 0 : 1,
                                                                matrix[yi - 1, xi - 1] == 255 ? 0 : 1,
                                                                matrix[yi, xi - 1] == 255 ? 0 : 1,
                                                                matrix[yi + 1, xi - 1] == 255 ? 0 : 1,
                                                                matrix[yi + 1, xi] == 255 ? 0 : 1,
                                                                matrix[yi + 1, xi + 1] == 255 ? 0 : 1,

                                                            };

                                    int cn = 0;
                                    for (int i = 0; i < values.Count; i++)
                                    {
                                        int idx = i;
                                        if (i == 7)
                                            idx = -1;
                                        cn += Math.Abs(values[i] - values[idx + 1]);
                                    }
                                    cn = (int)(cn * 0.5);


                                    double angleminu;
                                    // end minutiae
                                    if (cn == 1)
                                    {
                                        angleminu = GetMinutiaeAngle(matrix, xi, yi, MinutiaType.End);
                                        if (angleminu != -1)
                                            minutiaes.Add(new Minutia
                                            {
                                                Angle = (float)angleminu,
                                                X = (short)xi,
                                                Y = (short)yi,
                                                MinutiaType = MinutiaType.End
                                            }
                                                );
                                    }
                                    if (cn == 3)
                                    {
                                        angleminu = GetMinutiaeAngle(matrix, xi, yi, MinutiaType.Bifurcation);
                                        if (!double.IsNaN(angleminu) && angleminu != -1)
                                            minutiaes.Add(new Minutia
                                            {
                                                Angle = (float)angleminu,
                                                X = (short)xi,
                                                Y = (short)yi,
                                                MinutiaType = MinutiaType.Bifurcation
                                            }
                                                );
                                    }
                                }
                            }
                }

        List<Minutia> noInTheBorder = new List<Minutia>();

        for (int i = 0; i < minutiaes.Count; i++)
        {
            int row, col;
            orientationImage.GetBlockCoordFromPixel(minutiaes[i].X, minutiaes[i].Y, out row, out col);
            if (row >= 1 && col >= 1 && col < orientationImage.Width - 1 && row < orientationImage.Height - 1)
                if (!
                    (orientationImage.IsNullBlock(row - 1, col) ||
                        orientationImage.IsNullBlock(row + 1, col) ||
                        orientationImage.IsNullBlock(row, col - 1) ||
                        orientationImage.IsNullBlock(row, col + 1) //||
                    )
                    )
                    noInTheBorder.Add(minutiaes[i]);

        }

        MtiaEuclideanDistance miEuclideanDistance = new MtiaEuclideanDistance();

        bool[] toErase = new bool[noInTheBorder.Count];
        for (int i = 0; i < noInTheBorder.Count; i++)
        {
            Minutia mA = noInTheBorder[i];
            for (int j = 0; j < noInTheBorder.Count; j++)
            {
                if (i != j)
                {
                    Minutia mB = noInTheBorder[j];
                    int row, col;
                    orientationImage.GetBlockCoordFromPixel(mA.X, mA.Y, out row, out col);
                    double angleOI = orientationImage.AngleInRadians(row, col);
                    if (mA.MinutiaType == MinutiaType.End &&
                        Math.Min(Angle.DifferencePi(mA.Angle, angleOI),
                                    Angle.DifferencePi(mA.Angle, angleOI + Math.PI)) > Math.PI / 6)
                        toErase[i] = true;

                    if (mA.MinutiaType == mB.MinutiaType &&
                        miEuclideanDistance.Compare(mA, mB) < 5)
                        toErase[i] = toErase[j] = true;

                    if (mA.MinutiaType == MinutiaType.End &&
                        mB.MinutiaType == MinutiaType.End &&
                        mA.Angle == mB.Angle &&
                        miEuclideanDistance.Compare(mA, mB) < 10)
                        toErase[i] = toErase[j] = true;

                    if (mA.MinutiaType == MinutiaType.End &&
                        mB.MinutiaType == MinutiaType.End &&
                        Angle.DifferencePi(mA.Angle, mB.Angle) < Math.PI / 12 &&
                        miEuclideanDistance.Compare(mA, mB) < 10)
                        toErase[i] = toErase[j] = true;

                    if (mA.MinutiaType == MinutiaType.End &&
                        mB.MinutiaType == MinutiaType.Bifurcation &&
                        miEuclideanDistance.Compare(mA, mB) < 15)
                        if (RemoveSpikeOnMinutiae(matrix, mA, mB))
                            toErase[i] = true;
                }

            }
        }

        List<Minutia> result = new List<Minutia>();
        for (int i = 0; i < noInTheBorder.Count; i++)
            if (!toErase[i])
                result.Add(noInTheBorder[i]);

        return result;

    }

    private double GetMinutiaeAngle(ImageMatrix matrix, int x, int y, MinutiaType type)
    {
        double angle = 0;

        if (type == MinutiaType.End)
        {
            List<Point> points = new List<Point> { new Point(x, y) };
            for (int i = 0; i < 10; i++)
            {
                List<Point> neighbors = GetNeighboors(matrix, points[points.Count - 1].X, points[points.Count - 1].Y);
                foreach (var neighbor in neighbors)
                    if (!points.Contains(neighbor))
                        points.Add(neighbor);
            }
            if (points.Count < 10)
                return -1;
            angle = Angle.ComputeAngle(points[points.Count - 1].X - points[0].X,
                                        points[points.Count - 1].Y - points[0].Y);
        }

        if (type == MinutiaType.Bifurcation)
        {
            List<Point> treeNeighboors = GetNeighboors(matrix, x, y);

            if (treeNeighboors.Count < 3)
                return double.NaN;

            List<Point> n1 = new List<Point>() { new Point(x, y), treeNeighboors[0] };
            List<Point> n2 = new List<Point>() { new Point(x, y), treeNeighboors[1] };
            List<Point> n3 = new List<Point>() { new Point(x, y), treeNeighboors[2] };

            for (int i = 0; i < 10; i++)
            {
                List<Point> neighboors1 = GetNeighboors(matrix, n1[n1.Count - 1].X, n1[n1.Count - 1].Y);
                foreach (var neighbor in neighboors1)
                    if (!n1.Contains(neighbor) && !treeNeighboors.Contains(neighbor))
                        n1.Add(neighbor);

                List<Point> neighboors2 = GetNeighboors(matrix, n2[n2.Count - 1].X, n2[n2.Count - 1].Y);
                foreach (var neighbor in neighboors2)
                    if (!n2.Contains(neighbor) && !treeNeighboors.Contains(neighbor))
                        n2.Add(neighbor);

                List<Point> neighboors3 = GetNeighboors(matrix, n3[n3.Count - 1].X, n3[n3.Count - 1].Y);
                foreach (var neighbor in neighboors3)
                    if (!n3.Contains(neighbor) && !treeNeighboors.Contains(neighbor))
                        n3.Add(neighbor);
            }

            if (n1.Count < 10 || n2.Count < 10 || n3.Count < 10)
                return -1;

            double angleNeighboors1 = Angle.ComputeAngle(n1[n1.Count - 1].X - n1[0].X, n1[n1.Count - 1].Y - n1[0].Y);
            double angleNeighboors2 = Angle.ComputeAngle(n2[n2.Count - 1].X - n2[0].X, n2[n2.Count - 1].Y - n2[0].Y);
            double angleNeighboors3 = Angle.ComputeAngle(n3[n3.Count - 1].X - n3[0].X, n3[n3.Count - 1].Y - n3[0].Y);

            double diff1 = Angle.DifferencePi(angleNeighboors1, angleNeighboors2);
            double diff2 = Angle.DifferencePi(angleNeighboors1, angleNeighboors3);
            double diff3 = Angle.DifferencePi(angleNeighboors2, angleNeighboors3);

            if (diff1 <= diff2 && diff1 <= diff3)
            {
                angle = angleNeighboors2 + diff1 / 2;
                if (angle > 2 * Math.PI)
                    angle -= 2 * Math.PI;
            }
            else if (diff2 <= diff1 && diff2 <= diff3)
            {
                angle = angleNeighboors1 + diff2 / 2;
                if (angle > 2 * Math.PI)
                    angle -= 2 * Math.PI;
            }
            else
            {
                angle = angleNeighboors3 + diff3 / 2;
                if (angle > 2 * Math.PI)
                    angle -= 2 * Math.PI;
            }
        }

        return angle;
    }

    private List<Point> GetNeighboors(ImageMatrix matrix, int x, int y)
    {
        List<Point> result = new List<Point>();

        if (matrix[y, x + 1] == 0)
            result.Add(new Point(x + 1, y));
        if (matrix[y - 1, x + 1] == 0)
            result.Add(new Point(x + 1, y - 1));
        if (matrix[y - 1, x] == 0)
            result.Add(new Point(x, y - 1));
        if (matrix[y - 1, x - 1] == 0)
            result.Add(new Point(x - 1, y - 1));
        if (matrix[y, x - 1] == 0)
            result.Add(new Point(x - 1, y));
        if (matrix[y + 1, x - 1] == 0)
            result.Add(new Point(x - 1, y + 1));
        if (matrix[y + 1, x] == 0)
            result.Add(new Point(x, y + 1));
        if (matrix[y + 1, x + 1] == 0)
            result.Add(new Point(x + 1, y + 1));

        return result;
    }

    private bool RemoveSpikeOnMinutiae(ImageMatrix matrix, Minutia end, Minutia bifurcation)
    {
        if (bifurcation.MinutiaType == MinutiaType.Bifurcation)
        {
            int xBifur = bifurcation.X;
            int yBifur = bifurcation.Y;

            List<Point> treeNeighboors = GetNeighboors(matrix, xBifur, yBifur);

            if (treeNeighboors.Count < 3)
                return false;

            List<Point> n1 = new List<Point> { new Point(xBifur, yBifur), treeNeighboors[0] };
            List<Point> n2 = new List<Point> { new Point(xBifur, yBifur), treeNeighboors[1] };
            List<Point> n3 = new List<Point> { new Point(xBifur, yBifur), treeNeighboors[2] };

            int xEnd = end.X;
            int yEnd = end.Y;

            for (int i = 0; i < 15; i++)
            {
                List<Point> neighboors1 = GetNeighboors(matrix, n1[n1.Count - 1].X, n1[n1.Count - 1].Y);
                foreach (var neighbor in neighboors1)
                    if (!n1.Contains(neighbor) && !treeNeighboors.Contains(neighbor))
                        if (neighbor.X == xEnd &&
                            neighbor.Y == yEnd)
                            return true;
                        else
                            n1.Add(neighbor);

                List<Point> neighboors2 = GetNeighboors(matrix, n2[n2.Count - 1].X, n2[n2.Count - 1].Y);
                foreach (var neighbor in neighboors2)
                    if (!n2.Contains(neighbor) && !treeNeighboors.Contains(neighbor))
                        if (neighbor.X == xEnd &&
                            neighbor.Y == yEnd)
                            return true;
                        else
                            n2.Add(neighbor);

                List<Point> neighboors3 = GetNeighboors(matrix, n3[n3.Count - 1].X, n3[n3.Count - 1].Y);
                foreach (var neighbor in neighboors3)
                    if (!n3.Contains(neighbor) && !treeNeighboors.Contains(neighbor))
                        if (neighbor.X == xEnd &&
                            neighbor.Y == yEnd)
                            return true;
                        else
                            n3.Add(neighbor);
            }
        }
        return false;
    }

    private FingerSkeImgExtractor skeImgExtractor = new FingerSkeImgExtractor();

    #endregion
}



public class FingerOrImgExtractor : FeatureExtractor<OrientationImage>
{
    public byte BlockSize
    {
        get { return bSize; }
        set { bSize = value; }
    }

    public override OrientationImage ExtractFeatures(Bitmap image)
    {
        ImageMatrix matrix = new ImageMatrix(image);

        ImageMatrix Gx = yFilter.Apply(matrix);
        ImageMatrix Gy = xFilter.Apply(matrix);

        byte width = Convert.ToByte(image.Width / BlockSize);
        byte height = Convert.ToByte(image.Height / BlockSize);
        OrientationImage oi = new OrientationImage(width, height, BlockSize);
        for (int row = 0; row < height; row++)
            for (int col = 0; col < width; col++)
            {
                int x, y;
                oi.GetPixelCoordFromBlock(row, col, out x, out y);
                int x0 = Math.Max(x - BlockSize / 2, 0);
                int x1 = Math.Min(image.Width - 1, x + BlockSize / 2);
                int y0 = Math.Max(y - BlockSize / 2, 0);
                int y1 = Math.Min(image.Height - 1, y + BlockSize / 2);

                int Gxy = 0, Gxx = 0, Gyy = 0;
                for (int yi = y0; yi <= y1; yi++)
                    for (int xi = x0; xi <= x1; xi++)
                    {
                        Gxy += Gx[yi, xi] * Gy[yi, xi];
                        Gxx += Gx[yi, xi] * Gx[yi, xi];
                        Gyy += Gy[yi, xi] * Gy[yi, xi];
                    }

                if (Gxx - Gyy == 0 && Gxy == 0)
                    oi[row, col] = OrientationImage.Null;
                else
                {
                    double angle = Angle.ToDegrees(Angle.ComputeAngle(Gxx - Gyy, 2 * Gxy));
                    angle = angle / 2 + 90;
                    if (angle > 180)
                        angle = angle - 180;
                    oi[row, col] = Convert.ToByte(Math.Round(angle));
                }
            }

        RemoveBadBlocksVariance(oi, matrix);
        RemoveBadBlocks(oi);
        OrientationImage smoothed = SmoothOrImg(oi);
        return smoothed;
    }

    #region private

    private void RemoveBadBlocksVariance(OrientationImage oi, ImageMatrix matrix)
    {
        int maxLength = oi.WindowSize / 2;
        int[,] varianceMatrix = new int[oi.Height, oi.Width];
        double max = 0;
        double min = double.MaxValue;
        for (int row = 0; row < oi.Height; row++)
            for (int col = 0; col < oi.Width; col++)
            {
                int x, y;
                oi.GetPixelCoordFromBlock(row, col, out x, out y);

                int sum = 0;
                int count = 0;
                for (int xi = x - maxLength; xi < x + maxLength; xi++)
                    for (int yi = y - maxLength; yi < y + maxLength; yi++)
                        if (xi >= 0 && xi < matrix.Width && yi >= 0 && yi < matrix.Height)
                        {
                            sum += matrix[yi, xi];
                            count++;
                        }
                double avg = 1.0 * sum / count;

                double sqrSum = 0;
                for (int xi = x - maxLength; xi < x + maxLength; xi++)
                    for (int yi = y - maxLength; yi < y + maxLength; yi++)
                        if (xi >= 0 && xi < matrix.Width && yi >= 0 && yi < matrix.Height)
                        {
                            double diff = matrix[yi, xi] - avg;
                            sqrSum += diff * diff;
                        }
                varianceMatrix[row, col] = Convert.ToInt32(Math.Round(sqrSum / (count - 1)));

                if (varianceMatrix[row, col] > max)
                    max = varianceMatrix[row, col];
                if (varianceMatrix[row, col] < min)
                    min = varianceMatrix[row, col];
            }

        for (int row = 0; row < oi.Height; row++)
            for (int col = 0; col < oi.Width; col++)
                varianceMatrix[row, col] = Convert.ToInt32(Math.Round(254.0 * (varianceMatrix[row, col] - min) / (max - min)));

        const int t = 15;
        for (int row = 0; row < oi.Height; row++)
            for (int col = 0; col < oi.Width; col++)
                if (!oi.IsNullBlock(row, col) && varianceMatrix[row, col] <= t)
                    oi[row, col] = OrientationImage.Null;
    }

    private void RemoveBadBlocks(OrientationImage oi)
    {
        int[,] neighborsMatrix = new int[oi.Height, oi.Width];
        for (int row0 = 0; row0 < oi.Height; row0++)
            for (int col0 = 0; col0 < oi.Width; col0++)
                if (oi[row0, col0] != OrientationImage.Null)
                {
                    int lowRow = Math.Max(0, row0 - 1);
                    int lowCol = Math.Max(0, col0 - 1);
                    int highRow = Math.Min(row0 + 1, oi.Height - 1);
                    int highCol = Math.Min(col0 + 1, oi.Width - 1);
                    for (int row1 = lowRow; row1 <= highRow; row1++)
                        for (int col1 = lowCol; col1 <= highCol; col1++)
                            if (oi[row1, col1] != OrientationImage.Null)
                                neighborsMatrix[row0, col0]++;
                }

        for (int row0 = 0; row0 < oi.Height; row0++)
            for (int col0 = 0; col0 < oi.Width; col0++)
                if (oi[row0, col0] != OrientationImage.Null)
                {
                    bool shouldRemove = true;
                    int lowRow = Math.Max(0, row0 - 1);
                    int lowCol = Math.Max(0, col0 - 1);
                    int highRow = Math.Min(row0 + 1, oi.Height - 1);
                    int highCol = Math.Min(col0 + 1, oi.Width - 1);
                    for (int row1 = lowRow; row1 <= highRow && shouldRemove; row1++)
                        for (int col1 = lowCol; col1 <= highCol && shouldRemove; col1++)
                            if (neighborsMatrix[row1, col1] == 9)
                                shouldRemove = false;
                    if (shouldRemove)
                        oi[row0, col0] = OrientationImage.Null;
                }
    }

    private OrientationImage SmoothOrImg(OrientationImage img)
    {
        OrientationImage smoothed = new OrientationImage(img.Width, img.Height, img.WindowSize);
        double xSum, ySum, xAvg, yAvg, angle;
        int count;
        byte wSize = 3;
        for (int row = 0; row < img.Height; row++)
            for (int col = 0; col < img.Width; col++)
                if (!img.IsNullBlock(row, col))
                {
                    xSum = ySum = count = 0;
                    for (int y = row - wSize / 2; y <= row + wSize / 2; y++)
                        for (int x = col - wSize / 2; x <= col + wSize / 2; x++)
                            if (y >= 0 && y < img.Height && x >= 0 && x < img.Width && !img.IsNullBlock(y, x))
                            {
                                angle = img.AngleInRadians(y, x);
                                xSum += Math.Cos(2 * angle);
                                ySum += Math.Sin(2 * angle);
                                count++;
                            }
                    if (count == 0 || (xSum == 0 && ySum == 0))
                        smoothed[row, col] = OrientationImage.Null;
                    else
                    {
                        xAvg = xSum / count;
                        yAvg = ySum / count;
                        angle = Angle.ToDegrees(Angle.ComputeAngle(xAvg, yAvg)) / 2;

                        smoothed[row, col] = Convert.ToByte(Math.Round(angle));
                    }
                }
                else
                    smoothed[row, col] = OrientationImage.Null;

        return smoothed;
    }

    private SobelHorizontalFilter xFilter = new SobelHorizontalFilter();

    private SobelVerticalFilter yFilter = new SobelVerticalFilter();

    private byte bSize = 16;

    #endregion

}





public class FingerSkeImgExtractor : FeatureExtractor<SkeletonImage>
{
    #region public

    public override SkeletonImage ExtractFeatures(Bitmap image)
    {
        FingerOrImgExtractor FingerOrImgExtractor = new FingerOrImgExtractor();
        OrientationImage orientationImage = FingerOrImgExtractor.ExtractFeatures(image);
        ImageMatrix skeletonImage = ExtractSkeletonImage(image, orientationImage);

        byte[] img = new byte[skeletonImage.Width * skeletonImage.Height];
        for (int i = 0; i < skeletonImage.Height; i++)
            for (int j = 0; j < skeletonImage.Width; j++)
                img[skeletonImage.Width * i + j] = (byte)skeletonImage[i, j];

        return new SkeletonImage(img, skeletonImage.Width, skeletonImage.Height);
    }

    public ImageMatrix ExtractSkeletonImage(Bitmap image, OrientationImage orientationImage)
    {
        ImageMatrix matrix = new ImageMatrix(image);

        GaussianBlur gb = new GaussianBlur();
        matrix = gb.Apply(matrix);

        matrix = GetBinaryImage(matrix, orientationImage);
        ApplyThinning(matrix, orientationImage);
        RemoveSpikes(matrix, orientationImage);
        FixBifurcations(matrix, orientationImage);
        RemoveSmallSegments(matrix, orientationImage);

        return matrix;
    }

    #endregion

    #region private

    private ImageMatrix GetBinaryImage(ImageMatrix matrix, OrientationImage orientationImage)
    {
        int[] filter = new int[] { 1, 2, 5, 7, 5, 2, 1 };
        ImageMatrix newImg = new ImageMatrix(matrix.Width, matrix.Height);
        for (int i = 0; i < matrix.Width; i++)
            for (int j = 0; j < matrix.Height; j++)
            {
                newImg[j, i] = 255;
            }
        for (int row = 0; row < orientationImage.Height; row++)
            for (int col = 0; col < orientationImage.Width; col++)
                if (!orientationImage.IsNullBlock(row, col))
                {
                    int x, y;
                    orientationImage.GetPixelCoordFromBlock(row, col, out x, out y);

                    int maxLength = orientationImage.WindowSize / 2;
                    for (int xi = x - maxLength; xi < x + maxLength; xi++)
                        for (int yi = y - maxLength; yi < y + maxLength; yi++)
                        {
                            int[] projection = GetProjection(orientationImage, row, col, xi, yi, matrix);

                            int[] smoothed = new int[orientationImage.WindowSize + 1];
                            const int n = 7;
                            for (int j = 0; j < projection.Length; j++)
                            {
                                int idx = 0;
                                int sum = 0, count = 0;
                                for (int k = j - n / 2; k <= j + n / 2; k++, idx++)
                                    if (k >= 0 && k < projection.Length)
                                    {
                                        sum += projection[k] * filter[idx];
                                        count++;
                                    }
                                smoothed[j] = sum / count;
                            }

                            int center = smoothed.Length / 2;
                            int left;
                            for (left = center - 1; smoothed[left] == smoothed[center] && left > 0; left--) ;

                            int rigth;
                            for (rigth = center + 1; smoothed[rigth] == smoothed[center] && rigth < smoothed.Length - 1; rigth++) ;

                            if (xi >= 0 && xi < matrix.Width && yi >= 0 && yi < matrix.Height)
                                newImg[yi, xi] = 255;

                            if (xi > 0 && xi < matrix.Width - 1 && yi > 0 && yi < matrix.Height - 1 && !(left == 255 && rigth == smoothed.Length - 1))
                            {
                                if (smoothed[center] < smoothed[left] && smoothed[center] < smoothed[rigth])
                                    newImg[yi, xi] = 0;
                                else if (rigth - left == 2 &&
                                            ((smoothed[left] < smoothed[left - 1] &&
                                            smoothed[left] < smoothed[center]) ||
                                    (smoothed[rigth] < smoothed[rigth + 1] &&
                                            smoothed[rigth] < smoothed[center]) ||
                                    (smoothed[center] < smoothed[left - 1] &&
                                            smoothed[center] < smoothed[rigth + 1]) ||
                                            (smoothed[center] < smoothed[left - 1] &&
                                            smoothed[center] < smoothed[rigth]) ||
                                            (smoothed[center] < smoothed[left] &&
                                            smoothed[center] < smoothed[rigth + 1])))
                                    newImg[yi, xi] = 0;
                            }
                        }
                }
        return newImg;
    }

    private int[] GetProjection(OrientationImage oi, int row, int col, int x, int y, ImageMatrix matrix)
    {
        double angle = oi.AngleInRadians(row, col);
        double orthogonalAngle = oi.AngleInRadians(row, col) + Math.PI / 2;

        int maxLength = oi.WindowSize / 2;
        int[] projection = new int[2 * maxLength + 1];
        int[] outlayerCount = new int[2 * maxLength + 1];
        bool outlayerFound = false;
        int totalSum = 0;
        int validPointsCount = 0;
        for (int li = -maxLength, i = 0; li <= maxLength; li++, i++)
        {
            int xi = Convert.ToInt32(x - li * Math.Cos(orthogonalAngle));
            int yi = Convert.ToInt32(y - li * Math.Sin(orthogonalAngle));

            int ySum = 0;
            for (int lj = -maxLength; lj <= maxLength; lj++)
            {
                int xj = Convert.ToInt32(xi - lj * Math.Cos(angle));
                int yj = Convert.ToInt32(yi - lj * Math.Sin(angle));
                if (xj >= 0 && yj >= 0 && xj < matrix.Width && yj < matrix.Height)
                {
                    ySum += matrix[yj, xj];
                    validPointsCount++;
                }
                else
                {
                    outlayerCount[i]++;
                    outlayerFound = true;
                }
            }
            projection[i] = ySum;
            totalSum += ySum;
        }

        if (outlayerFound)
        {
            int avg = totalSum / validPointsCount;
            for (int i = 0; i < projection.Length; i++)
                projection[i] += avg * outlayerCount[i];
        }

        return projection;
    }

    private void ApplyThinning(ImageMatrix matrix, OrientationImage orientationImage)
    {
        bool changed = true;
        while (changed)
        {
            changed = false;
            for (int row = 0; row < orientationImage.Height; row++)
                for (int col = 0; col < orientationImage.Width; col++)
                    if (!orientationImage.IsNullBlock(row, col))
                    {
                        int x, y;
                        orientationImage.GetPixelCoordFromBlock(row, col, out x, out y);

                        int maxLength = orientationImage.WindowSize / 2;
                        for (int xi = x - maxLength; xi < x + maxLength; xi++)
                            for (int yi = y - maxLength; yi < y + maxLength; yi++)
                                if (xi > 0 && xi < matrix.Width - 1 && yi > 0 && yi < matrix.Height - 1)
                                {
                                    int tl = matrix[yi - 1, xi - 1];
                                    int tc = matrix[yi - 1, xi];
                                    int tr = matrix[yi - 1, xi + 1];

                                    int le = matrix[yi, xi - 1];
                                    int ce = matrix[yi, xi];
                                    int ri = matrix[yi, xi + 1];

                                    int bl = matrix[yi + 1, xi - 1];
                                    int bc = matrix[yi + 1, xi];
                                    int br = matrix[yi + 1, xi + 1];

                                    if (IsVL(tl, tc, tr, le, ce, ri, bl, bc, br) ||
                                        IsVR(tl, tc, tr, le, ce, ri, bl, bc, br) ||
                                        IsHT(tl, tc, tr, le, ce, ri, bl, bc, br) ||
                                        IsHB(tl, tc, tr, le, ce, ri, bl, bc, br))
                                    {
                                        matrix[yi, xi] = 255;
                                        changed = true;
                                    }
                                }
                                else
                                    matrix[yi, xi] = 255;
                    }
        }
    }

    private void RemoveSpikes(ImageMatrix matrix, OrientationImage orientationImage)
    {
        for (int row = 0; row < orientationImage.Height; row++)
            for (int col = 0; col < orientationImage.Width; col++)
                if (!orientationImage.IsNullBlock(row, col))
                {
                    double[] cos = new double[3];
                    double[] sin = new double[3];

                    double orthogonalAngle = orientationImage.AngleInRadians(row, col) + Math.PI / 2;
                    cos[0] = Math.Cos(orthogonalAngle);
                    sin[0] = Math.Sin(orthogonalAngle);

                    double orthogonalAngle1 = orthogonalAngle + Math.PI / 12;
                    cos[1] = Math.Cos(orthogonalAngle1);
                    sin[1] = Math.Sin(orthogonalAngle1);

                    double orthogonalAngle2 = orthogonalAngle - Math.PI / 12;
                    cos[2] = Math.Cos(orthogonalAngle2);
                    sin[2] = Math.Sin(orthogonalAngle2);

                    int x, y;
                    orientationImage.GetPixelCoordFromBlock(row, col, out x, out y);

                    int maxLength = orientationImage.WindowSize / 2;
                    for (int xi = x - maxLength; xi < x + maxLength; xi++)
                        for (int yi = y - maxLength; yi < y + maxLength; yi++)
                        {
                            int xj = xi;
                            int yj = yi;
                            bool spikeFound = true;
                            while (spikeFound)
                            {
                                spikeFound = false;
                                if (xj > 0 && xj < matrix.Width - 1 && yj > 0 && yj < matrix.Height - 1)
                                {
                                    int tl = matrix[yj - 1, xj - 1];
                                    int tc = matrix[yj - 1, xj];
                                    int tr = matrix[yj - 1, xj + 1];

                                    int le = matrix[yj, xj - 1];
                                    int ce = matrix[yj, xj];
                                    int ri = matrix[yj, xj + 1];

                                    int bl = matrix[yj + 1, xj - 1];
                                    int bc = matrix[yj + 1, xj];
                                    int br = matrix[yj + 1, xj + 1];

                                    if (CouldBeSpike(tl, tc, tr, le, ce, ri, bl, bc, br))
                                        for (int i = 0; i < sin.Length && !spikeFound; i++)
                                        {
                                            int xk = Convert.ToInt32(Math.Round(xj - cos[i]));
                                            int yk = Convert.ToInt32(Math.Round(yj - sin[i]));
                                            if (matrix[yk, xk] == 0)
                                            {
                                                matrix[yj, xj] = 255;
                                                xj = xk;
                                                yj = yk;
                                                spikeFound = true;
                                            }
                                            else
                                            {
                                                xk = Convert.ToInt32(Math.Round(xj + cos[i]));
                                                yk = Convert.ToInt32(Math.Round(yj + sin[i]));
                                                if (matrix[yk, xk] == 0)
                                                {
                                                    matrix[yj, xj] = 255;
                                                    xj = xk;
                                                    yj = yk;
                                                    spikeFound = true;
                                                }
                                            }
                                        }
                                }
                            }
                        }
                }
    }

    private void FixBifurcations(ImageMatrix matrix, OrientationImage orientationImage)
    {
        bool changed = true;
        while (changed)
        {
            changed = false;
            for (int row = 0; row < orientationImage.Height; row++)
                for (int col = 0; col < orientationImage.Width; col++)
                    if (!orientationImage.IsNullBlock(row, col))
                    {
                        int x, y;
                        orientationImage.GetPixelCoordFromBlock(row, col, out x, out y);

                        int maxLength = orientationImage.WindowSize / 2;
                        for (int xi = x - maxLength; xi < x + maxLength; xi++)
                            for (int yi = y - maxLength; yi < y + maxLength; yi++)
                                if (xi > 0 && xi < matrix.Width - 1 && yi > 0 && yi < matrix.Height - 1)
                                {
                                    int tl = matrix[yi - 1, xi - 1];
                                    int tc = matrix[yi - 1, xi];
                                    int tr = matrix[yi - 1, xi + 1];

                                    int le = matrix[yi, xi - 1];
                                    int ce = matrix[yi, xi];
                                    int ri = matrix[yi, xi + 1];

                                    int bl = matrix[yi + 1, xi - 1];
                                    int bc = matrix[yi + 1, xi];
                                    int br = matrix[yi + 1, xi + 1];

                                    if (IsCorner(tl, tc, tr, le, ce, ri, bl, bc, br))
                                    {
                                        matrix[yi, xi] = 255;
                                        changed = true;
                                    }
                                }
                                else
                                    matrix[yi, xi] = 255;
                    }
        }
    }

    private void RemoveSmallSegments(ImageMatrix matrix, OrientationImage orientationImage)
    {
        for (int row = 0; row < orientationImage.Height; row++)
            for (int col = 0; col < orientationImage.Width; col++)
                if (!orientationImage.IsNullBlock(row, col))
                {
                    int x, y;
                    orientationImage.GetPixelCoordFromBlock(row, col, out x, out y);

                    int maxLength = orientationImage.WindowSize / 2;
                    for (int xi = x - maxLength; xi < x + maxLength; xi++)
                        for (int yi = y - maxLength; yi < y + maxLength; yi++)
                        {
                            const int pThreshold = 10;
                            int[] xArr = new int[pThreshold + 1];
                            int[] yArr = new int[pThreshold + 1];

                            int x0, y0;
                            if (IsEnd(matrix, xi, yi, out x0, out y0))
                            {
                                xArr[0] = xi;
                                yArr[0] = yi;

                                xArr[1] = x0;
                                yArr[1] = y0;

                                bool endFound = false;
                                bool bifurcationFound = false;
                                int pCount = 1;
                                for (int i = 1; i < pThreshold && !endFound && !bifurcationFound; i++)
                                {
                                    if (IsEnd(matrix, xArr[i], yArr[i], out xArr[i + 1], out yArr[i + 1]))
                                        endFound = true;
                                    else if (!IsContinous(matrix, xArr[i - 1], yArr[i - 1], xArr[i], yArr[i],
                                                            out xArr[i + 1], out yArr[i + 1]))
                                        bifurcationFound = true;

                                    pCount++;
                                }
                                if (endFound || (bifurcationFound && pCount <= pThreshold))
                                    for (int i = 0; i < pCount; i++)
                                        matrix[yArr[i], xArr[i]] = 255;
                            }
                        }
                }
    }

    private bool IsVR(int tl, int tc, int tr, int le, int ce, int ri, int bl, int bc, int br)
    {
        if (tl == 0 && tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 0 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 0 && br == 255
            )
            return true;
        if (tl == 0 && tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 0 && br == 255
            )
            return true;
        if (tl == 0 && tc == 0 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tc == 0 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 0 && br == 255
            )
            return true;

        return false;
    }

    private bool IsVL(int tl, int tc, int tr, int le, int ce, int ri, int bl, int bc, int br)
    {
        if (tl == 255 && tc == 255 && tr == 0 &&
                        le == 255 && ce == 0 && ri == 0 &&
                        bl == 255 && bc == 255 && br == 0
                        )
            return true;
        if (tl == 0 && tc == 0 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255 && br == 0
            )
            return true;
        if (tl == 255 && tc == 0 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tl == 255 && tc == 0 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 0 && br == 0
            )
            return true;
        if (tl == 255 && tc == 0 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 0 && br == 0
            )
            return true;

        return false;
    }

    private bool IsHB(int tl, int tc, int tr, int le, int ce, int ri, int bl, int bc, int br)
    {
        if (tl == 0 && tc == 0 && tr == 0 &&
                        le == 255 && ce == 0 && ri == 255 &&
                        bl == 255 && bc == 255 && br == 255
                        )
            return true;
        if (tl == 0 && tc == 0 && tr == 0 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tc == 0 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tc == 0 && tr == 0 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tc == 0 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tc == 0 && tr == 0 &&
            le == 0 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        return false;
    }

    private bool IsHT(int tl, int tc, int tr, int le, int ce, int ri, int bl, int bc, int br)
    {
        if (tl == 255 && tc == 255 && tr == 255 &&
                        le == 255 && ce == 0 && ri == 255 &&
                        bl == 0 && bc == 0 && br == 0
                        )
            return true;
        if (tl == 255 && tc == 255 && tr == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 0 && bc == 0 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 0 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 0 && bc == 0 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
            bl == 0 && bc == 0 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 0 &&
            bl == 0 && bc == 0 && br == 0
            )
            return true;
        return false;
    }

    private bool CouldBeSpike(int tl, int tc, int tr, int le, int ce, int ri, int bl, int bc, int br)
    {
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 255 && ce == 0 && ri == 255 &&
                            bc == 0
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
            le == 255 && ce == 0 &&
            bl == 255 && br == 0
            )
            return true;
        if (tl == 255 && tc == 255 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255
            )
            return true;
        if (tl == 255 && tr == 0 &&
            le == 255 && ce == 0 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tc == 0 &&
            le == 255 && ce == 0 && ri == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tl == 0 && tr == 255 &&
                            ce == 0 && ri == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
            return true;
        if (tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
                        bc == 255 && br == 255
            )
            return true;
        if (tl == 255 && tc == 255 && tr == 255 &&
                            ce == 0 && ri == 255 &&
            bl == 0 && br == 255
            )
            return true;

        return false;
    }

    private bool IsCorner(int tl, int tc, int tr, int le, int ce, int ri, int bl, int bc, int br)
    {
        if (tl == 255 && tc == 255 &&
            le == 255 && ce == 0 && ri == 0 &&
             bc == 0
            )
            return true;

        if (tc == 0 &&
            le == 255 && ce == 0 && ri == 0 &&
            bl == 255 && bc == 255
            )
            return true;

        if (tc == 0 &&
            le == 0 && ce == 0 && ri == 255 &&
             bc == 255 && br == 255
            )
            return true;

        if (tc == 255 && tr == 255 &&
            le == 0 && ce == 0 && ri == 255 &&
             bc == 0
            )
            return true;

        return false;
    }

    private bool IsEnd(ImageMatrix matrix, int x, int y, out int x1, out int y1)
    {
        x1 = -1;
        y1 = -1;

        int tl = (x > 0 && y > 0) ? matrix[y - 1, x - 1] : 255;
        int tc = (y > 0) ? matrix[y - 1, x] : 255;
        int tr = (x < matrix.Width - 1 && y > 0) ? matrix[y - 1, x + 1] : 255;
        int cl = (x > 0) ? matrix[y, x - 1] : 255;
        int ce = matrix[y, x];
        int cr = (x < matrix.Width - 1) ? matrix[y, x + 1] : 255;
        int bl = (x > 0 && y < matrix.Height - 1) ? matrix[y + 1, x - 1] : 255;
        int bc = (y < matrix.Height - 1) ? matrix[y + 1, x] : 255;
        int br = (x < matrix.Width - 1 && y < matrix.Height - 1) ? matrix[y + 1, x + 1] : 255;

        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x;
            y1 = y;
            return true;
        }
        if (tl == 0 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x - 1;
            y1 = y - 1;
            return true;
        }
        if (tl == 255 && tc == 0 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x;
            y1 = y - 1;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 0 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x + 1;
            y1 = y - 1;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 0 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x + 1;
            y1 = y;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 0
            )
        {
            x1 = x + 1;
            y1 = y + 1;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 0 && br == 255
            )
        {
            x1 = x;
            y1 = y + 1;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 0 && bc == 255 && br == 255
            )
        {
            x1 = x - 1;
            y1 = y + 1;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 0 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x - 1;
            y1 = y;
            return true;
        }
        return false;
    }

    private bool IsContinous(ImageMatrix matrix, int x0, int y0, int x, int y, out int x1, out int y1)
    {
        x1 = -1;
        y1 = -1;
        bool isBlack = false;
        if (matrix[y0, x0] == 0)
        {
            matrix[y0, x0] = 255;
            isBlack = true;
        }

        int tl = (x > 0 && y > 0) ? matrix[y - 1, x - 1] : 255;
        int tc = (y > 0) ? matrix[y - 1, x] : 255;
        int tr = (x < matrix.Width - 1 && y > 0) ? matrix[y - 1, x + 1] : 255;
        int cl = (x > 0) ? matrix[y, x - 1] : 255;
        int ce = matrix[y, x];
        int cr = (x < matrix.Width - 1) ? matrix[y, x + 1] : 255;
        int bl = (x > 0 && y < matrix.Height - 1) ? matrix[y + 1, x - 1] : 255;
        int bc = (y < matrix.Height - 1) ? matrix[y + 1, x] : 255;
        int br = (x < matrix.Width - 1 && y < matrix.Height - 1) ? matrix[y + 1, x + 1] : 255;

        if (tl == 0 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x - 1;
            y1 = y - 1;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 0 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x;
            y1 = y - 1;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 0 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x + 1;
            y1 = y - 1;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 0 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x + 1;
            y1 = y;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 0
            )
        {
            x1 = x + 1;
            y1 = y + 1;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 0 && br == 255
            )
        {
            x1 = x;
            y1 = y + 1;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 255 && ce == 0 && cr == 255 &&
            bl == 0 && bc == 255 && br == 255
            )
        {
            x1 = x - 1;
            y1 = y + 1;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        if (tl == 255 && tc == 255 && tr == 255 &&
            cl == 0 && ce == 0 && cr == 255 &&
            bl == 255 && bc == 255 && br == 255
            )
        {
            x1 = x - 1;
            y1 = y;
            if (isBlack)
                matrix[y0, x0] = 0;
            return true;
        }
        return false;
    }

    #endregion
}

