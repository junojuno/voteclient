﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    class Election
    {
        public int idx { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string state { get; set; }
    }
}
