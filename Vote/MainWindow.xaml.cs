﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            VoteQueue.getInstance().start();

            if (VoteQueue.chkInternetStatus())
            {
                startReceiveP2P();
                //Debug.WriteLine(Network.getInstance().restoreNodes().ToString());

            }
            //InternetConnectionChecker.getInstance().start();

            //Debug.WriteLine(Network.getInstance().restoreNodes().ToString());
            this.SizeToContent = SizeToContent.WidthAndHeight;

        }

        public void startReceiveP2P()
        {
            P2PReceiver.startReceive();
            new BroadcastReceiver(44444).start();
        }
        
    }
}
