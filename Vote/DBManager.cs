﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    class DBManager
    {
        public const string DATABASE_NAME = "vote.db";
        private const string TABLE_NAME_BLOCK = "blocks";
        private const string TABLE_NAME_TRANSACTION = "transactions";
        private const string TABLE_NAME_ELECTION = "elections";
        private const string TABLE_NAME_CANDIDATE = "candidates";

        public const string CONNECTION_STRING = "Data Source=" + DATABASE_NAME + ";Pooling=true;FailIfMissing=False";

        private const string QUERY_CNT_TXES = "SELECT COUNT(*) FROM transactions";
        private const string QUERY_PREV_HASH = "SELECT hash FROM blocks ORDER BY id DESC LIMIT 1";
        private const string QUERY_CHECK_TABLE_EXIST = "SELECT COUNT(*) cnt FROM sqlite_master WHERE name = '{0}'";

        private const string QUERY_PROB_TXES = "SELECT id, sign, candidate, prev_id, block_id FROM transactions WHERE id BETWEEN {0} and {1}";

        private const string QUERY_DELETE_ELECTIONS = "DELETE FROM elections";
        private const string QUERY_DELETE_CANDIDATES = "DELETE FROM candidates";
        private const string QUERY_DELETE_BLKS = "DELETE FROM blocks";
        private const string QUERY_DELETE_TXES = "DELETE FROM transactions";

        private const string QUERY_SELECT_BLOCKS = "SELECT id, prev_hash FROM blocks";

        private const string QUERY_SELECT_TXES_ALL = "SELECT candidate, sign, prev_id, block_id FROM transactions";
        private const string QUERY_SELECT_TXES = "SELECT candidate, sign, prev_id, block_id FROM transactions WHERE block_id = {0}";

        private const string QUERY_SELECT_ELECTIONS = "SELECT * FROM elections";
        private const string QUERY_SELECT_CANDIDATES = "SELECT * FROM candidates WHERE idx_elections = {0}";

        private const string QUERY_ADD_BLOCK = "INSERT INTO blocks(hash, prev_hash) VALUES('{0}','{1}')";
        public const string QUERY_ADD_TRANSACTION = "INSERT INTO transactions(sign, candidate, prev_id, reg_date, block_id) VALUES('{0}','{1}', {2}, '{3}', {4})";

        private const string QUERY_UPDATE_ELECTION = "INSERT OR REPLACE INTO elections(id, type, title, state) VALUES({0},'{1}', '{2}', '{3}')";
        private const string QUERY_UPDATE_CANDIDATE = "INSERT OR REPLACE INTO candidates(id, name, promise, info, idx_elections, img, number) VALUES({0},'{1}', '{2}', '{3}', {4}, '{5}', {6})";

        private const string QUERY_CREATE_TABLE_ELECTION = "CREATE TABLE elections (id INTEGER PRIMARY KEY, type TEXT, title TEXT, state TEXT)";
        private const string QUERY_CREATE_TABLE_CANDIDATE = "CREATE TABLE candidates (id INTEGER PRIMARY KEY, name TEXT, promise TEXT, info TEXT, idx_elections INTEGER, img TEXT, number INTEGER)";

        private const string QUERY_CREATE_TABLE_BLOCK = "CREATE TABLE blocks (id INTEGER PRIMARY KEY NOT NULL, hash VARCHAR(512), prev_hash VARCHAR(512), trans_hash VARCHAR(512), nonce INTEGER)";
        private const string QUERY_CREATE_TABLE_TRANSACTION = "CREATE TABLE transactions (id INTEGER PRIMARY KEY, prev_id INTEGER, reg_date TEXT, sign TEXT, candidate TEXT, block_id INTEGER)";


        private static DBManager instance = null;

        private SQLiteConnection con;

        DBManager()
        {
            init();
        }  

        ~DBManager()
        {
            //close();
        }

        public void close()
        {
            con.Close();
                
        }

        public static DBManager getInstance()
        {
            if (instance == null)
            {
                instance = new DBManager();
            }

            return instance;
        }

        public void init()
        {
            con = new SQLiteConnection(CONNECTION_STRING);
            con.Open();

            if (!isExistTable(TABLE_NAME_BLOCK))
            {
                Debug.WriteLine("NOT EXIST TABLE " + TABLE_NAME_BLOCK);
                createTable(QUERY_CREATE_TABLE_BLOCK);
                insertBlock("ffffffffffffffff", "");
            }

            if (!isExistTable(TABLE_NAME_TRANSACTION))
            {
                Debug.WriteLine("NOT EXIST TABLE " + TABLE_NAME_TRANSACTION);
                createTable(QUERY_CREATE_TABLE_TRANSACTION);
            }

            if (!isExistTable(TABLE_NAME_ELECTION))
            {
                Debug.WriteLine("NOT EXIST TABLE " + TABLE_NAME_ELECTION);
                createTable(QUERY_CREATE_TABLE_ELECTION);
            }

            if (!isExistTable(TABLE_NAME_CANDIDATE))
            {
                Debug.WriteLine("NOT EXIST TABLE " + TABLE_NAME_CANDIDATE);
                createTable(QUERY_CREATE_TABLE_CANDIDATE);
            }

            ArrayList list = GetTables();

            foreach (var item in list) 
            {
                Debug.WriteLine("list = " + item.ToString());
            }
        }

        private SQLiteCommand getCommand(string query)
        {
            SQLiteCommand command = new SQLiteCommand(query, con);
            return command;
        }

        private bool isExistTable(string tableName)
        {
            string strExistCmd = String.Format(QUERY_CHECK_TABLE_EXIST, tableName);
            SQLiteCommand cmdExist = getCommand(strExistCmd);
            return Convert.ToInt32(cmdExist.ExecuteScalar()) > 0;
        }

        public List<TX> selectTxesAll()
        {

            SQLiteCommand command = getCommand(QUERY_SELECT_TXES_ALL);

            SQLiteDataReader reader = command.ExecuteReader();

            List<TX> txes = new List<TX>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {

                    Debug.WriteLine("!! " + reader.GetString(0) +" / "+ reader.GetString(1) + " / " + reader.GetInt32(2) + " / " + reader.GetInt32(3));
                    TX tx = new TX(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3));
                    txes.Add(tx);
                }
            }

            return txes;
        }

        private List<TX> selectTxes(int block_id)
        {

            SQLiteCommand command = getCommand(String.Format(QUERY_SELECT_TXES, block_id));

            SQLiteDataReader reader = command.ExecuteReader();

            List<TX> txes = new List<TX>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    
                    TX tx = new TX(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3));
                    txes.Add(tx);
                }
            }

            return txes;
        }

        public List<Block> selectBlocks()
        {

            SQLiteCommand command = getCommand(QUERY_SELECT_BLOCKS);

            SQLiteDataReader reader = command.ExecuteReader();

            List<Block> blocks = new List<Block>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int blockId = reader.GetInt32(0);
                    List<TX> txes = selectTxes(blockId);
                    Block block = BlockManager.getInstance().makeBlock(reader.GetString(1), txes);
                    
                }
            }

            return blocks;
        }

        private int createTable(string query)
        {
            SQLiteCommand command = getCommand(query);
            return command.ExecuteNonQuery();
        }

        private int CURDOperation (string query)
        {
            Debug.WriteLine("Query = " + query);
            SQLiteCommand command = getCommand(query);
            return command.ExecuteNonQuery();
        }

        public int cntTxes()
        {
            SQLiteCommand command = getCommand(QUERY_CNT_TXES);

            SQLiteDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                return reader.GetInt32(0);
            }

            return 0;
        }

        public int deleteBlks()
        {
            return CURDOperation(QUERY_DELETE_BLKS);
        }

        public int deleteTxes()
        {
            return CURDOperation(QUERY_DELETE_TXES);
        }

        public int deleteElections()
        {
            return CURDOperation(QUERY_DELETE_ELECTIONS);
        }
        
        
        public int deleteCandidates()
        {
            return CURDOperation(QUERY_DELETE_CANDIDATES);
        }

        public List<Election> selectElections()
        {
            SQLiteCommand command = getCommand(QUERY_SELECT_ELECTIONS);

            SQLiteDataReader reader = command.ExecuteReader();

            List<Election> elections = new List<Election>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Election election = new Election();
                    election.idx = reader.GetInt32(0);
                    election.type = reader.GetString(1);
                    election.title = reader.GetString(2);
                    election.state = reader.GetString(3);

                    elections.Add(election);

                }
            }

            return elections;
        }

        public List<Candidate> selectCandidates(int election_idx)
        {
            SQLiteCommand command = getCommand(
                String.Format(QUERY_SELECT_CANDIDATES, election_idx)
                );

            SQLiteDataReader reader = command.ExecuteReader();

            List<Candidate> candidates = new List<Candidate>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Candidate candiadate = new Candidate();
                    candiadate.idx = reader.GetInt32(0);
                    candiadate.name = reader.GetString(1);
                    candiadate.promise = reader.GetString(2);
                    candiadate.info = reader.GetString(3);
                    candiadate.idx_elections = reader.GetInt32(4);
                    candiadate.img = reader.GetString(5);
                    candiadate.number = reader.GetInt32(6);
                    
                    candidates.Add(candiadate);
                    
                }
            }

            return candidates;
        }

        public int insertTransaction(TX tx)
        {
            return CURDOperation(String.Format(QUERY_ADD_TRANSACTION, tx.Sign, tx.PubKey, tx.prevId, "test", tx.block_id));
        }

        public int insertBlock(string hash, string prevHash) 
        {
            return CURDOperation(String.Format(QUERY_ADD_BLOCK, hash, prevHash));
        }

        public int insertElection(string id, string type, string title, string state)
        {
            return CURDOperation(String.Format(QUERY_UPDATE_ELECTION, id, type, title, state));
        }

        public int insertCandidate(string id, string name, string promise, string info, string idx_elections, string img, string number)
        {
            return CURDOperation(String.Format(QUERY_UPDATE_CANDIDATE, id, name, promise, info, idx_elections, img, number));
        }

        public string prevHash()
        {
            SQLiteCommand command = getCommand(QUERY_PREV_HASH);

            SQLiteDataReader reader = command.ExecuteReader();
            
            if (reader.HasRows)
            {
                reader.Read();
                return reader.GetString(0);
                
            }

            return "ffffff";
        }

        public List<TX> getTxBetween(int start, int end)
        {

            string query = String.Format(QUERY_PROB_TXES, start, end);

            Debug.WriteLine(query);

            SQLiteCommand command = getCommand(query);

            SQLiteDataReader reader = command.ExecuteReader();
            
            if (reader.HasRows)
            {

                List<TX> txes = new List<TX>();


                while (reader.Read())
                {
                    TX tx = new TX(reader.GetString(2), reader.GetString(1), reader.GetInt32(3), reader.GetInt32(4));
                    txes.Add(tx);
                }

                return txes;
            }

            return null;
        }

        public ArrayList GetTables()
        {
            ArrayList list = new ArrayList();

            // executes query that select names of all tables in master table of the database
            String query = "SELECT name FROM sqlite_master " +
                    "WHERE type = 'table'" +
                    "ORDER BY 1";
            try
            {

                DataTable table = GetDataTable(query);

                // Return all table names in the ArrayList

                foreach (DataRow row in table.Rows)
                {
                    Debug.WriteLine("Print ALll = " + row.ItemArray[0].ToString());
                    list.Add(row.ItemArray[0].ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return list;
        }

        public DataTable GetDataTable(string sql)
        {
            try
            {
                DataTable dt = new DataTable();
             
                using (SQLiteCommand cmd = new SQLiteCommand(sql, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        dt.Load(rdr);
                        return dt;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

    }
}
