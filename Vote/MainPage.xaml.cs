﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vote
{
    /// <summary>
    /// MainPage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            initPage();
        }

        public void initPage()
        {

            bool status = true;

            Thread t = new Thread(new ThreadStart(
                () =>
                {

                    status = VoteQueue.chkInternetStatus();

                    Debug.WriteLine("Internet Status is " + status);
                    if (status)
                    {
                        updateNodeInfo();
                        mergeVoteDatas();
                    }
                    else
                    {

                        //new P2PSender(tbAddress.Text, tbDatta.Text, P2PSender.COMMAND_SEND_FILE).RunWorkerAsync();
                    }


                }
            ));

            t.Start();


            /*t.Join();
            
            if (!status)
            {
                btnNewVote.IsEnabled = false;
                btnRegCandidate.IsEnabled = false;
                btnRegFinger.IsEnabled = false;
            }
            */
        }

        public void mergeVoteDatas()
        {
            DBManager.getInstance().deleteCandidates();
            DBManager.getInstance().deleteElections();
            mergeElectionList();
            mergeCandidateList();

            if (isLatest())
            {
                Debug.WriteLine("IsLatest");
            }
            else
            {
                Debug.WriteLine("IsLatest No....");
                /*
                JObject job = Network.getInstance().restoreNodes();
                JArray jar = JArray.Parse(job.First.First.ToString());

                foreach (JToken child in jar.Children())
                {
                    NameValueCollection collection = new NameValueCollection();

                    foreach (JToken grandChild in child.Children())
                    {
                        JProperty prp = grandChild as JProperty;
                        collection.Add(prp.Name, prp.Value.ToString());
                        Debug.WriteLine(prp.Name +" ? " + prp.Value);

                    }

                    string addr = collection.Get("address");

                    new P2PSender(addr, null, P2PSender.COMMAND_SEND_FILE).RunWorkerAsync();
                }
                */
            }

        }

        public static bool isLatest()
        {
            JObject job = new Network().latestHash();
            NameValueCollection collection = new NameValueCollection();

            foreach (JToken token in job.First)
            {
                foreach(JProperty prop in token.Children())
                {
                    collection.Add(prop.Name, prop.Value.ToString());

                    Debug.WriteLine(prop.Name +" / " + prop.Value);
                }
            }
            

            return (collection.Get("hash").Length == 0 || DBManager.getInstance().prevHash().Equals(collection.Get("hash")) ) && DBManager.getInstance().cntTxes() == Int32.Parse(collection.Get("cnt_txes")) ;
                
        }

        public void mergeElectionList()
        {
            JObject job = new Network().electionList();
            JArray jar = JArray.Parse(job.First.First.ToString());

            foreach (JToken child in jar.Children())
            {
                NameValueCollection vals = new NameValueCollection();

                foreach (JToken grandChild in child)
                {
                    var property = grandChild as JProperty;

                    if (property != null)
                    {
                        vals.Add(property.Name, property.Value.ToString());
                    }
                }

                DBManager.getInstance().insertElection(vals.Get("idx"), vals.Get("type"), vals.Get("title"), vals.Get("state"));
            }
        }

        public void mergeCandidateList()
        {
            JObject job = new Network().allCandidate();
            JArray jar = JArray.Parse(job.First.First.ToString());

            foreach (JToken child in jar.Children())
            {
                NameValueCollection vals = new NameValueCollection();

                foreach (JToken grandChild in child)
                {
                    var property = grandChild as JProperty;

                    if (property != null)
                    {
                        vals.Add(property.Name, property.Value.ToString());
                    }
                }

                DBManager.getInstance().insertCandidate(vals.Get("idx"), vals.Get("name"), vals.Get("promise"), vals.Get("info"), vals.Get("idx_elections"), vals.Get("img"), vals.Get("number"));
            }
        }

        public string getLocalIpAddressByWeb()
        {
            string searchIpFromUrl = new System.Net.WebClient().DownloadString(("http://checkip.dyndns.org"));

            //자를부분
            string EtcIpInfo = searchIpFromUrl.Substring(searchIpFromUrl.IndexOf("</body>"), searchIpFromUrl.Length - searchIpFromUrl.IndexOf("</body>"));

            //전체에서 시작점~ 전체길이-앞뒤자를부분
            string serverIp = searchIpFromUrl.Substring(searchIpFromUrl.IndexOf(":") + 1, searchIpFromUrl.Length - searchIpFromUrl.IndexOf(":") - EtcIpInfo.Length - 1).Trim();

            return serverIp;
        }

        // Deprecated 
        public string getLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            string localIp = "127.0.0.1";

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    //MessageBox.Show("ip = " + ip);
                    localIp = ip.ToString();
                }
            }

            return localIp;
        }
        
        private void updateNodeInfo()
        {
            JObject job = Network.getInstance().updateNodeInfo(DBManager.getInstance().prevHash());//getLocalIpAddressByWeb());
            //MessageBox.Show(job.ToString());
        }

        private void btnVote_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ElectionListPage(AuthPage.Redirect.VOTE));
        }

        private void btnResult_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ElectionListPage(AuthPage.Redirect.RES));
            //NavigationService.Navigate(new Uri("ResultPage.xaml", UriKind.Relative));
        }

        private void btnNewVote_Click(object sender, RoutedEventArgs e)
        {
            
            string title = Microsoft.VisualBasic.Interaction.InputBox("선거 제목을 입력해주세요.", "투표 만들기", "새로운 투표", -1, -1);

           // List<Key> keylist = KeyManager.getInstance().genKeyList(10);

           // foreach(Key key in keylist) {
          //      Debug.WriteLine("Key is = " + key.PubKey);
        //    }

       //     List<TX> txes = TXManager.getInstance().genTXList(keylist);

      //      foreach (TX tx in txes)
            //{
              //  Debug.WriteLine("Tx is = " + tx.PubKey);
               // DBManager.getInstance().insertTransaction(tx);
            //}

            JObject res = Network.getInstance().newElection(title);
            initPage();

     //       Network.getInstance().updateNodeInfo(res.First.First.First.First.ToString(), keylist);
        }

        private void btnRegFinger_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ElectionListPage(AuthPage.Redirect.REG_VOTER));
        }

        private void btnRegCandidate_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ElectionListPage(AuthPage.Redirect.REG_CANDIDATE));
        }
    }
}
