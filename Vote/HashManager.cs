﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;
namespace Vote
{
   
    class HashManager
    {
        private static HashManager instance = null;
        public static HashManager getInstance()
        {
            if (instance == null)
            {
                instance = new HashManager();
                
            }

            return instance;
        }

        public byte[] S256Hash(string str)
        {

            return S256Hash(Encoding.UTF8.GetBytes(str));
        }
        public byte[] S256Hash(byte[] bytearr)
        {

            SHA256 sha = new SHA256Managed();
            byte[] hash = sha.ComputeHash(bytearr);
            return hash;
        }

        // Convert an object to a byte array
        public static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        // Convert a byte array to an Object
        private static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);

            return obj;
        }
    }


}
