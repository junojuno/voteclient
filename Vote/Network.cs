﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;

namespace Vote
{
    class Network
    {
        public const int P2P_PORT = 44444;

        public const int HASH_UPDATE_PORT = 44444;

        public const String BASE_SERVER_URL = "http://211.189.19.45";
        public const String BASE_SERVER_PORT = "8080";

        public const String BASE_URL = BASE_SERVER_URL + ":" + BASE_SERVER_PORT + "/";
        public const String APP_NAME = "vote/";
        public const String RESOURCE_PATH = "resources/";

        public const String SERVER_URL = BASE_URL + APP_NAME;
        public const String RESOURCES_URL = SERVER_URL + RESOURCE_PATH;

        public const String METHOD_GET = "GET";
        public const String METHOD_POST = "POST";
        public const String METHOD_PUT = "PUT";

        public const String VOTE_ELECTION_LIST = "elections/";
        public const String VOTE_CANDIDATE_LIST = "candidates/";
        public const String VOTE_SERVER_ALIVE = "info/alive";
        public const String VOTE_UPDATE_NODE_INFO = "node/";

        public const String ELECTION_NEW = "elections";
        public const String CANDIDATE_ALL = "candidates/all";
        public const String VOTER_ADD = "voter";
        public const String VOTE = "voter/vote";
        

        public const String BLOCK_CHAIN_ADD_BLOCK= "hashs";
        public const String BLOCK_CHAIN_LATEST = "hashs/latest";
        public const String BLOCK_JUDGE = "hashs/judge";
        public const String BLOCK_JUDGE_RES = "hashs/judge/res";

        public const String RESTORE_RESTORE_NODES = "restores/nodes";
        public const String VOTE_CHECK_EXIST_NODE = "node/exist";

        public const String CANDIDATE_ADD = "candidates";

        private static Network instance;

        public object JsonConvert { get; private set; }

        public static Network getInstance()
        {
            if (instance == null)
            {
                instance = new Network();
            }

            return instance;
        }

        public string getStrParams(Hashtable hash)
        {
            string strParams = null;

            if (hash != null)
            {
                IDictionaryEnumerator hashEnum = hash.GetEnumerator();
                DictionaryEntry entry;

                while (hashEnum.MoveNext())
                {
                    entry = (DictionaryEntry)hashEnum.Current;
                    strParams += entry.Key + "=" + entry.Value + "&";
                }

                strParams = strParams.Substring(0, strParams.Length - 1);

            }

            return strParams;
        }

        public JObject getRestResponseByJson(String restUrl, String method, Hashtable hash)
        {
            string url = SERVER_URL + restUrl;
            string strParams = getStrParams(hash);

            WebRequest request;
            byte[] byteData = null;

            switch (method)
            {
                case METHOD_POST:
                    request = WebRequest.Create(url);

                    if (strParams != null)
                    {
                        byteData = UTF8Encoding.UTF8.GetBytes(strParams);
                        request.ContentLength = byteData.Length;
                    }

                    request.ContentType = "application/x-www-form-urlencoded";
                    
                    break;

                case METHOD_GET :
                default :
                    if (strParams != null)
                    {
                        request = WebRequest.Create(url + "?" + strParams);
                    }
                    else
                    {
                        request = WebRequest.Create(url);
                    }
                    
                    break;
            }

            request.Method = method;
            string result = getResponse(request, byteData, method);

            return JObject.Parse(result);
        }

        public string getResponse(WebRequest request, byte[] datas, String method)
        {
            try
            {

                if (datas != null && datas.Length > 0 && (method.Equals(METHOD_POST) || method.Equals(METHOD_PUT)))
                {
                    Stream reqStream = request.GetRequestStream();
                    reqStream.Write(datas, 0, datas.Length);
                    reqStream.Close();
                }

                WebResponse response = request.GetResponse();
                Stream resStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);

                string strResponse = reader.ReadToEnd();

                closeAll(response, resStream, reader);

                return strResponse;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        public void closeAll(WebResponse response, Stream resStream, StreamReader reader)
        {
            response.Close();
            resStream.Close();
            reader.Close();
        }
        
        public JObject latestHash()
        {
            return getRestResponseByJson(BLOCK_CHAIN_LATEST, METHOD_GET, null);
        }

        public JObject electionList()
        {
            return getRestResponseByJson(VOTE_ELECTION_LIST, METHOD_GET, null);
        }

        public JObject candidateList(int election_idx)
        {
            return getRestResponseByJson(VOTE_ELECTION_LIST + election_idx + "/" + VOTE_CANDIDATE_LIST, METHOD_GET, null);
        }

        public JObject restoreNodes()
        {
            return getRestResponseByJson(RESTORE_RESTORE_NODES, METHOD_GET, null);
        }

        public JObject serverAlive()
        {
            return getRestResponseByJson(VOTE_SERVER_ALIVE, METHOD_GET, null);
        }
        

        public JObject updateNodeInfo(string tailHash)
        {
            Hashtable hash = new Hashtable();
            hash.Add("tail_hash", tailHash);
            return getRestResponseByJson(VOTE_UPDATE_NODE_INFO, METHOD_POST, hash);
        }

        public JObject vote(string name, string sign, int election_idx, int candidate_idx, String fingerprint)
        {
            Hashtable hash = new Hashtable();
            hash.Add("name", name);
            hash.Add("sign", sign);
            hash.Add("election_idx", election_idx);
            hash.Add("candidate_idx", candidate_idx);
            hash.Add("fingerprint", fingerprint);
            return getRestResponseByJson(VOTE, METHOD_POST, hash);
        }


        public JObject newElection(string title)
        {
            Hashtable hash = new Hashtable();
            hash.Add("title", title);
            return getRestResponseByJson(ELECTION_NEW, METHOD_POST, hash);
        }

        public JObject allCandidate()
        {
            return getRestResponseByJson(CANDIDATE_ALL, METHOD_GET, null);
        }

        public JObject addVoter(string name, string fingerprint, string elections_idx)
        {
            Hashtable hash = new Hashtable();
            hash.Add("name", name);
            hash.Add("fingerprint", fingerprint);
            hash.Add("elections_idx", elections_idx);
            return getRestResponseByJson(VOTER_ADD, METHOD_POST, hash);
        }

        
        public JObject judge(string hash, string prev_hash, string nonce, String prob)
        {
            Hashtable param = new Hashtable();
            param.Add("hash", hash);
            param.Add("prev_hash", prev_hash);
            param.Add("nonce", nonce);
            param.Add("prob", prob);
            return getRestResponseByJson(BLOCK_JUDGE, METHOD_POST, param);
        }

        public JObject judgeRes(int prob, int isRight)
        {
            Hashtable hash = new Hashtable();
            hash.Add("prob", prob);
            hash.Add("isRight", isRight);
            return getRestResponseByJson(BLOCK_JUDGE_RES, METHOD_POST, hash);
        }

        public JObject addCandidate(NameValueCollection values, NameValueCollection files)
        {
            return JObject.Parse(sendMultipartData(SERVER_URL + CANDIDATE_ADD, values, files));
        }


        private string sendMultipartData(string url, NameValueCollection values, NameValueCollection files = null)
        {
            Debug.WriteLine(url);
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            Stream requestStream = request.GetRequestStream();

            foreach (string key in values.Keys)
            {
                // Write item to stream
                byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                requestStream.Write(formItemBytes, 0, formItemBytes.Length);
            }

            if (files != null)
            {
                foreach (string key in files.Keys)
                {
                    if (File.Exists(files[key]))
                    {
                        int bytesRead = 0;
                        byte[] buffer = new byte[2048];
                        byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                        requestStream.Write(formItemBytes, 0, formItemBytes.Length);

                        using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                        {
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }

                            fileStream.Close();
                        }
                    }
                }
            }

            // Write trailer and close stream
            requestStream.Write(trailer, 0, trailer.Length);
            requestStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            };
        }
    }
}
