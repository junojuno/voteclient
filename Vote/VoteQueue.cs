﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Vote
{
    class VoteQueue
    {

        private static VoteQueue instance;
        private Timer timer;
        private Queue<EventHandler> works;

        private bool status = true;
        private bool prevStatus = true;
        private static bool restored = false;

        private VoteQueue()
        {
            timer = new Timer();
            timer.Interval = 1000;
            timer.Elapsed += new ElapsedEventHandler(checkLoop);

            works = new Queue<EventHandler>();
        }

        public void addQueueWork(EventHandler work)
        {
            works.Enqueue(work);
        }

        public void start()
        {
            timer.Start();
        }

        public static VoteQueue getInstance()
        {
            if (instance == null)
            {
                instance = new VoteQueue();
            }

            return instance;
        }

        void checkLoop(object sender, ElapsedEventArgs e)
        {
            bool tmpStatus = status;
            status = chkInternetStatus();
            prevStatus = tmpStatus;

            if (status && works.Count > 0)
            {
                EventHandler work = works.Dequeue();
                work(null, null);
            }

            if (prevStatus == false && status == true && restored == false)
            {
                restored = true;
                prevStatus = true;
                status = true;
                Debug.WriteLine("Restore!");

                if (MainPage.isLatest())
                {
                    Debug.WriteLine("is Latest!");
                }
                else
                {
                    JObject job = Network.getInstance().restoreNodes();
                    Debug.WriteLine("Restore Node Data is " + job);
                    JArray jar = JArray.Parse(job.First.First.ToString());

                    foreach (JToken child in jar.Children())
                    {
                        NameValueCollection col = new NameValueCollection();

                        foreach(JToken grandChild in child.Children())
                        {
                            JProperty prop = grandChild as JProperty;
                            col.Add(prop.Name, prop.Value.ToString());
                        }

                        string addr = col.Get("address");

                        //new P2PSender(addr, null, P2PSender.COMMAND_SEND_FILE).RunWorkerAsync();
                        break;
                    }
                }

                Task task = Task.Delay(4000).ContinueWith(t => VoteQueue.resstored());
            }
        }

        static void resstored()
        {
            restored = false;
        }
        
        public static bool chkInternetStatus()
        {
          
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
