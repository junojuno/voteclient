﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vote
{
    class ImageProcessor
    {

        private static ImageProcessor instance = null;

        private const int IMAGE_WIDTH = 256;
        private const int IMAGE_HEIGHT = 288;

        //int[,] picture = new int[IMAGE_WIDTH, IMAGE_HEIGHT];

        private SerialPort arduSerialPort = new SerialPort();


        private ImageProcessor()
        {
            initialSetting("COM7", 57600);
        }

        ~ImageProcessor()
        {
            arduSerialPort.Close();
        }

        public static ImageProcessor getInstance()
        {
            if (instance == null)
            {
                instance = new ImageProcessor();
            }

            return instance;
        }

        /*  
         *  NAME        :   CHANGE SENSOR SETTING (DATA LENGTH)
         *  DESCRIPT    :   change packet size 128 setting
         *                  in sensor
         *  INPUT       :   none
         *  OUTPUT      :   conformation code
         */
        public int settingPacketLength128()
        {
            arduSerialPort.Write(new byte[] { 0xEF }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x01 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x01 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x00 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x05 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x0e }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x06 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x02 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x00 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x1c }, 0, 1);

            int len = readPacketInform();
            int[] packet = new int[len];
            for (int i = 0; i < len; i++)
            {
                packet[i] = arduSerialPort.ReadByte();
            }
            Console.Write("conformation code is : ");
            Console.WriteLine(packet[0]);

            return packet[0];

        }





        /*
        ***SERIAL_TEST_SETTING_MODULES_END************************************************************************************* 
        */





        /*
        ***SERIAL_COMMUNICATE_MODULES_START************************************************************************************* 
        */







        /*  
         *  NAME        :   PAKET WRITE MODULE
         *  DESCRIPT    :   packet write to fingerprint module
         *                  address is default 0xffffffff
         *  INPUT       :   instruction code, checksum
         *  OUTPUT      :   none
         */
        public int emptyBuffer()
        {
            arduSerialPort.Write(new byte[] { 0xEF }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x01 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x01 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x00 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x03 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x0d }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x00 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x11 }, 0, 1);

            int head1 = arduSerialPort.ReadByte();
            int head2 = arduSerialPort.ReadByte();
            int addr1 = arduSerialPort.ReadByte();
            int addr2 = arduSerialPort.ReadByte();
            int addr3 = arduSerialPort.ReadByte();
            int addr4 = arduSerialPort.ReadByte();
            int paketId = arduSerialPort.ReadByte();
            int packetLen1 = arduSerialPort.ReadByte();
            int packetLen2 = arduSerialPort.ReadByte();
            int packetLen = ((packetLen1 << 4) + packetLen2);

            int conformCode = arduSerialPort.ReadByte();

            int chkSum1 = arduSerialPort.ReadByte();
            int chkSum2 = arduSerialPort.ReadByte();
            //Console.WriteLine(conformCode);
            return conformCode;
        }







        /*  
         *  NAME        :   PAKET WRITE MODULE
         *  DESCRIPT    :   packet write to fingerprint module
         *                  address is default 0xffffffff
         *  INPUT       :   instruction code, checksum
         *  OUTPUT      :   none
         */
        public void writePacket(byte instructionCode, byte checkSum)
        {
            arduSerialPort.Write(new byte[] { 0xEF }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x01 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0xff }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x01 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x00 }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x03 }, 0, 1);
            arduSerialPort.Write(new byte[] { instructionCode }, 0, 1);
            arduSerialPort.Write(new byte[] { 0x00 }, 0, 1);
            arduSerialPort.Write(new byte[] { checkSum }, 0, 1);
        }


        /*  
         *  NAME        :   READ PACKET INFORMATION
         *  DESCRIPT    :   read paket inform and return packet length
         *                  address is default 0xffffffff
         *  INPUT       :   none
         *  OUTPUT      :   paket length
         */
        public int readPacketInform()
        {
            int head1 = arduSerialPort.ReadByte();
            int head2 = arduSerialPort.ReadByte();
            int addr1 = arduSerialPort.ReadByte();
            int addr2 = arduSerialPort.ReadByte();
            int addr3 = arduSerialPort.ReadByte();
            int addr4 = arduSerialPort.ReadByte();
            int paketId = arduSerialPort.ReadByte();
            int packetLen1 = arduSerialPort.ReadByte();
            int packetLen2 = arduSerialPort.ReadByte();
            int packetLen = ((packetLen1 << 4) + packetLen2);
            /*
            Console.WriteLine("---------------");
            Console.Write("paket ID is : ");
            Console.WriteLine(paketId);
            Console.Write("paket length is : ");
            Console.WriteLine(packetLen);
            */
            return packetLen;
        }


        /*  
         *  NAME        :   READ PACKET DATA
         *  DESCRIPT    :   process for empty value problem
         *                  
         *  INPUT       :   none
         *  OUTPUT      :   paket data(1byte)
         */
        public int readPacketData()
        {
            int data;
            while (true)
            {
                data = arduSerialPort.ReadByte();
                //        if (data != null)
                //      {
                //        return data;
                //  }
            }
        }





        /*  
         *  NAME        :   GenImg Command
         *  DESCRIPT    :   Generate finger Image command
         *                  from fingerprint sensor
         *  INPUT       :   none
         *  OUTPUT      :   comformation code (0 == success, 01 == error, 02 == can't detect finger, 03 == fail to collect finger)
         */
        public int genImg()
        {
            int len;
            writePacket(0x01, 0x05);
            len = readPacketInform();
            int[] packet = new int[len];
            for (int i = 0; i < len; i++)
            {
                packet[i] = arduSerialPort.ReadByte();
            }
            return packet[0];
        }


        /*  
         *  NAME        :   UpImage
         *  DESCRIPT    :   upload image from Img_Buffer to upper computer
         *                  
         *  INPUT       :   none
         *  OUTPUT      :   comformation code (0 == ready to transfer, 01 == error, 0f == fail)
         */
        public int upImage()
        {
            int len;
            writePacket(0x0A, 0x0E);
            len = readPacketInform();
            int[] packet = new int[len];
            for (int i = 0; i < len; i++)
            {
                packet[i] = arduSerialPort.ReadByte();
            }
            return packet[0];
        }






        /*
        ***SERIAL_COMMUNICATE_MODULES_END*************************************************************************************** 
        */



        /*
        ***FUNCTION_MODULES_START***********************************************************************************************
        */


        /*  
         *  NAME        :   String to Byte array
         *  DESCRIPT    :   make string array using byte array
         *                  
         *  INPUT       :   instruction code, checksum
         *  OUTPUT      :   none
         */
        public static byte[] StrToByteArray(string str)
        {
            Dictionary<string, byte> hexindex = new Dictionary<string, byte>();
            for (int i = 0; i <= 255; i++)
                hexindex.Add(i.ToString("X2"), (byte)i);

            List<byte> hexres = new List<byte>();
            for (int i = 0; i < str.Length; i += 2)
                hexres.Add(hexindex[str.Substring(i, 2)]);

            return hexres.ToArray();
        }



        /*  
         *  NAME        :   ImageRequestFunction
         *  DESCRIPT    :   function for request image
         *                  
         *  INPUT       :   none
         *  OUTPUT      :   none
         */
        public void imageRequest()
        {
            int confchk;
            Console.WriteLine("fingerprint waiting...");

            while (true)
            {
                confchk = genImg();
                if (confchk == 0)
                    break;
            }

            Console.WriteLine("fingerprint detected");

            while (true)
            {
                confchk = upImage();
                if (confchk == 0)
                    break;
            }
            Console.WriteLine("Uploading finger Image...");
        }





        /*  
         *  NAME        :   MakeImageDataArray
         *  DESCRIPT    :   function for save image as array
         *                  
         *  INPUT       :   Data array index, Array ref
         *  OUTPUT      :   amended Data array index
         */
        public int makeImageDataArray(ref int[,] arr, int dataIndex)
        {
            int len;
            int temp;
            len = readPacketInform();
            for (int i = 0; i < (len - 2) * 2; i++)
            {
                temp = arduSerialPort.ReadByte();
                arr[i, dataIndex] = (temp >> 4) * 16;
                i++;
                arr[i, dataIndex] = (temp & 15) * 16;
            }
            arduSerialPort.ReadByte();
            arduSerialPort.ReadByte();
            return dataIndex + 1;
        }




        /*  
         *  NAME        :   makeBitmapImage
         *  DESCRIPT    :   function for make image as bitmap
         *                  
         *  INPUT       :   picture array, bitmap array
         *  OUTPUT      :   amended Data array index
         */
        public Bitmap makeBitmapImage(ref int[,] arr, int width, int height)
        {
            var bitmap = new Bitmap(width, height, PixelFormat.Format16bppArgb1555);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int red = arr[x, y]; // read from array
                    int green = arr[x, y]; // read from array
                    int blue = arr[x, y]; // read from array
                    bitmap.SetPixel(x, y, Color.FromArgb(0, red, green, blue));
                }
            }
            return bitmap;
        }






        /*  
         *  NAME        :   imageSave
         *  DESCRIPT    :   function for save image as bitmap
         *                  
         *  INPUT       :   picture array, imageWidth, imageHeight, savePath(include name)
         *  OUTPUT      :   bitmap
         */
        public Bitmap imageSave(string savePath)
        {

            int[,] picture = new int[IMAGE_WIDTH, IMAGE_HEIGHT];

            int index = 0;
            for (int i = 0; i < IMAGE_HEIGHT ; i++)
            {
                index = makeImageDataArray(ref picture, index);
            }
            Bitmap bitmapImage = makeBitmapImage(ref picture, IMAGE_WIDTH, IMAGE_HEIGHT);
            bitmapImage.Save(savePath, System.Drawing.Imaging.ImageFormat.Bmp);
            return bitmapImage;
        }





        /*  
         *  NAME        :   initialSetting
         *  DESCRIPT    :   function for sensor connection initial setting
         *                  
         *  INPUT       :   combobox(port_select),bottonName, BaudRate
         *  OUTPUT      :   none
         */
        public void initialSetting(string portName, int BaudRate)
        {
            System.Threading.Thread.Sleep(1000);
            arduSerialPort.PortName = portName;
            arduSerialPort.BaudRate = BaudRate;
            arduSerialPort.Open();
            emptyBuffer();
        }




        /*
        ***FUNCTION_MODULES_END***********************************************************************************************
        */





        /*
        ***IMAGE_PROCESSING_MODULES_START***********************************************************************************************
        */
        /*

        public Bitmap histogramEqualization(Bitmap sourceImage)
        {
            Bitmap renderedImage = sourceImage;

            uint pixels = (uint)renderedImage.Height * (uint)renderedImage.Width;
            decimal Const = 255 / (decimal)pixels;

            int x, y, R, G, B;

            ImageStatistics statistics = new ImageStatistics(renderedImage);

            //Create histogram arrays for R,G,B channels
            int[] cdfR = statistics.Red.Values.ToArray();
            int[] cdfG = statistics.Green.Values.ToArray();
            int[] cdfB = statistics.Blue.Values.ToArray();

            //Convert arrays to cumulative distribution frequency data
            for (int r = 1; r <= 255; r++)
            {
                cdfR[r] = cdfR[r] + cdfR[r - 1];
                cdfG[r] = cdfG[r] + cdfG[r - 1];
                cdfB[r] = cdfB[r] + cdfB[r - 1];
            }

            for (y = 0; y < renderedImage.Height; y++)
            {
                for (x = 0; x < renderedImage.Width; x++)
                {
                    Color pixelColor = renderedImage.GetPixel(x, y);

                    R = (int)((decimal)cdfR[pixelColor.R] * Const);
                    G = (int)((decimal)cdfG[pixelColor.G] * Const);
                    B = (int)((decimal)cdfB[pixelColor.B] * Const);

                    Color newColor = Color.FromArgb(R, G, B);
                    renderedImage.SetPixel(x, y, newColor);
                }
            }
            return renderedImage;
        }


    */
    }
}
