﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Vote
{
    [Serializable]
    class TX
    {
        public int block_id { get; set; }
        private byte[] sign, publickey;
        private int prevID;
        private DateTime votedTime;

        public TX(String candidate, String sign, int prevID)
        {
            publickey = StringToByteArray(candidate);
            this.sign = StringToByteArray(sign);
            votedTime = DateTime.Now;
            this.prevID = prevID;
        }

        public TX(String candidate, String sign, int prevID, int block_id)
        {
            publickey = StringToByteArray(candidate);
            this.sign = StringToByteArray(sign);
            votedTime = DateTime.Now;
            this.block_id = block_id;
        }

        public int prevId
        {
            get { return prevID; }
        }
        public String PubKey
        {
            get
            {
                return ByteArrayToString(publickey);
            }
        }
        public String Sign
        {
            get
            {
                return ByteArrayToString(sign);
            }
        }
        public DateTime Time
        {
            get
            {
                return votedTime;
            }
        }

        public int verifySign()
        {
            //  if (eckey.VerifyData(StringToByteArray("voted"), StringToByteArray(sign))) 
            return 0;//data is good
            return -1;//data is wrong
        }


        static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }
        static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            for (int i = 0; i < ba.Length; i++)
                hex.Append(ba[i].ToString("X2"));
            return hex.ToString();
        }
        override public string ToString()
        {
            string str = "{";
            str += "sign : "+ByteArrayToString(sign)+", ";
            str += "publickey : " + ByteArrayToString(publickey) + ", ";
            str += "prevID : " + prevID + "}";
            return str;
        }

    }
}
