﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Vote
{
    class 
        BlockFinder : BlockManager
    {
        private static BlockFinder instance = null;
        private static Block block;

        public delegate void FindEventHandler(int nonce, string prevHash);
        public event FindEventHandler handler;

        public static int cntInit = 0;
        

        //블록의 해시값 찾기(백그라운드), 해시값을 찾으려는 블록을 넘겨줘야함
        public static BlockFinder startFind(Block b)
        {

            if (instance == null)
            {
                instance = new BlockFinder(block);
                block = b;
                instance.start();
            }
            else
            {
                
                if (instance.IsBusy) { 
                    instance.CancelAsync();
                }
                block = b;
                instance.start();
                
            }

            cntInit ++;

            return instance;
        }
        

        BlockFinder(Block block)
        {
            this.DoWork += new DoWorkEventHandler(find);
        }

        void find(object sender, DoWorkEventArgs e)
        {
            block = this.findHashRand(block);
            handler((int)(block.Nonce) , block.PrevHash);
            // block.header.nNonce <= 0 Failed To Find Block Nonce
            // TODO When Success Find Block
        }
        
        void start()
        {
            
            this.RunWorkerAsync();
        }
    }

}
